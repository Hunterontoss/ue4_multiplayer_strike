#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_ZoomCameraComponent.generated.h"


class ATW_BaseCharacter;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_STRIKE_API UTW_ZoomCameraComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UTW_ZoomCameraComponent();

protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditAnywhere, Category = Combat)
	float ZoomedFOV = 30.f;
	

	UPROPERTY(EditAnywhere, Category = Combat)
	float ZoomInterpSpeed = 20.f;

	void InterpFOV(float DeltaTime, const float FOVZoom, const float InterpSpeedZoom);

private:
	float CurrentFOV;
	float DefaultFOV;

	UPROPERTY()
	ATW_BaseCharacter* P_OwnerCharacter;
};
