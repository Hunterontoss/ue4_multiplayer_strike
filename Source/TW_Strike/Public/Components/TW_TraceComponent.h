#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_TraceComponent.generated.h"

#define TW_TRACE_LENGTH 80000.f

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_STRIKE_API UTW_TraceComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_TraceComponent();
	
protected:
	virtual void BeginPlay() override;

public:
	void TraceUnderCrosshairs(FHitResult& TraceHitResult);

private:
	bool GetScreenToWorld(FVector& CrosshairWorldPosition, FVector& CrosshairWorldDirection);

	FCollisionQueryParams MakeQueryParams();
};
