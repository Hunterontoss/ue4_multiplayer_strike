#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_HealthComponent.generated.h"

//DECLARE_MULTICAST_DELEGATE(FOnDeathSignature);
//DECLARE_MULTICAST_DELEGATE_TwoParams(FOnHealthChanged, float, float);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_STRIKE_API UTW_HealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_HealthComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	FORCEINLINE float GetHealth() const  { return Health; }
	FORCEINLINE float GetMaxHealth() const  { return MaxHealth; }

	//FOnDeathSignature OnDeath;
	//FOnHealthChanged OnHealthChanged;
	
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void ReceiveDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, class AController* InstigatorController, AActor* DamageCauser);

private:	
	UPROPERTY(EditAnywhere, Category = "Player Stats")
	float MaxHealth = 100.f;

	UPROPERTY(ReplicatedUsing = OnRep_Health, VisibleAnywhere, Category = "Player Stats")
	float Health = 100.f;

	UFUNCTION()
	void OnRep_Health();
};
