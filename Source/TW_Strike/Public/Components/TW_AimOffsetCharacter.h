#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_StrikeTypes/TurningInPlace.h"
#include "TW_AimOffsetCharacter.generated.h"

class ATW_BaseCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_STRIKE_API UTW_AimOffsetCharacter : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_AimOffsetCharacter();

	friend ATW_BaseCharacter;

	FORCEINLINE float GetAO_Yaw() const { return AO_Yaw; }
	FORCEINLINE float GetAO_Pitch() const { return AO_Pitch; }
	
protected:
	virtual void BeginPlay() override;

	void AimOffset(float DeltaTime);
	
private:

	UPROPERTY()
	ATW_BaseCharacter* Character;
	
	float AO_Yaw;
	float InterpAO_Yaw;
	float AO_Pitch;
	FRotator StartingAimRotation;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Turn In Place
	///
public:
	FORCEINLINE ETurningInPlace GetTurnInPlace() const { return TurningInPlace; } 
		
protected:
	void UpdateTurnInPlace(float DeltaTime);

private:
	ETurningInPlace TurningInPlace;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Fix Proxy Turn Sync
	///
	
public:
	void CalculateAO_Pitch();
	void SimProxiesTurn();

	FORCEINLINE bool ShouldRotateRootBone() const { return bRotateRootBone; }
	
private:
	bool bRotateRootBone;
	float TurnThreshold = 0.5f;
	FRotator ProxyRotationLastFrame;
	FRotator ProxyRotation;
	float ProxyYaw;
	float TimeSinceLastMovementReplication;
	
	float CalculateSpeed();
	////////////////////////////////////////////////////////////////////////////////////////
};
