// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_PickupComponent.generated.h"

class USphereComponent;
class UWidgetComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_STRIKE_API UTW_PickupComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_PickupComponent();

protected:
	virtual void BeginPlay() override;


	////////////////////////////////////////////////////////////////////////////////////////
	///  Pickup 
	///
public:	
	UPROPERTY(VisibleAnywhere, Category = Setup)
	USphereComponent* AreaSphere;
	
protected:
	UFUNCTION()
	virtual void OnSphereOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult
	);

	UFUNCTION()
	void OnSphereEndOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex
	);

};
