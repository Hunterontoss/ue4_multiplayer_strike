#pragma once
#include "TW_WeaponCore.generated.h"

UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	EWS_Initial UMETA(DisplayName = "Initial State"),
	EWS_Equipped UMETA(DisplayName = "Equipped"),
	EWS_Dropped UMETA(DisplayName = "Dropped"),

	EWS_MAX UMETA(DisplayName = "DefaultMAX")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	EWT_AssaultRifle UMETA(DisplayName = "Assault Rifle"),
	EWT_Pistol UMETA(DisplayName = "Pistol"),
	EWT_RocketLauncher UMETA(DisplayName = "Rocket"),
	EWT_SniperRifle UMETA(DisplayName = "SniperRifle"),
	
	EWT_MAX UMETA(DisplayName = "DefaultMAX")
};