#pragma once

#include "CoreMinimal.h"
#include "Weapon/TW_BaseWeapon.h"
#include "TW_HitScanWeapon.generated.h"

class UParticleSystem;

UCLASS()
class TW_STRIKE_API ATW_HitScanWeapon : public ATW_BaseWeapon
{
	GENERATED_BODY()

public:
	virtual void Fire(const FVector& HitTarget) override;

private:
	UPROPERTY(EditAnywhere)
	float Damage = 50.f;

	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactParticles;

	UPROPERTY(EditAnywhere)
	UParticleSystem* BeamParticles;
	
	AController* GetOwnerInstigatorController() const;

	void MakeDamage(const FHitResult& FireHit, AController* InstigatorController);
};
