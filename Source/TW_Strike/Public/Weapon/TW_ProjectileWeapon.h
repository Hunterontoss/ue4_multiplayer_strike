#pragma once

#include "CoreMinimal.h"
#include "Weapon/TW_BaseWeapon.h"
#include "TW_ProjectileWeapon.generated.h"

class ATW_BaseProjectile;

UCLASS()
class TW_STRIKE_API ATW_ProjectileWeapon : public ATW_BaseWeapon
{
	GENERATED_BODY()

public:
	virtual void Fire(const FVector& HitTarget) override;

private:
	UPROPERTY(EditAnywhere)
	TSubclassOf<ATW_BaseProjectile> ProjectileClass;
};
