#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TW_Casing.generated.h"

class USoundCue;

UCLASS()
class TW_STRIKE_API ATW_Casing : public AActor
{
	GENERATED_BODY()
	
public:	
	ATW_Casing();

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	
private:
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* CasingMesh;

	UPROPERTY(EditAnywhere)
	float ShellEjectionImpulse = 10.f;

	UPROPERTY(EditAnywhere)
	USoundCue* ShellSound;
};
