#pragma once

#include "CoreMinimal.h"
#include "Weapon/Projectiles/TW_BaseProjectile.h"
#include "TW_BulletProjectile.generated.h"

UCLASS()
class TW_STRIKE_API ATW_BulletProjectile : public ATW_BaseProjectile
{
	GENERATED_BODY()

protected:
	virtual void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
};
