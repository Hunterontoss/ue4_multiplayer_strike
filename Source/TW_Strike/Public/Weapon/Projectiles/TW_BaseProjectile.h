#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TW_BaseProjectile.generated.h"

class UBoxComponent;
class UProjectileMovementComponent;
class UParticleSystem;
class UParticleSystemComponent;
class USoundCue;

UCLASS()
class TW_STRIKE_API ATW_BaseProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	ATW_BaseProjectile();

	virtual void Tick(float DeltaTime) override;
	virtual void Destroyed() override;
	
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float LifeSeconds = 6.0f;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Collision 
	///
private:
	UPROPERTY(EditAnywhere)
	UBoxComponent* CollisionBox;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Movement 
	///
private:
	UPROPERTY(VisibleAnywhere)
	UProjectileMovementComponent* ProjectileMovementComponent;


	////////////////////////////////////////////////////////////////////////////////////////
	/// Trace Effect
	///
private:
	UPROPERTY(EditAnywhere)
	UParticleSystem* Tracer;

	UPROPERTY()
	UParticleSystemComponent* TracerComponent;
	////////////////////////////////////////////////////////////////////////////////////////
	/// Hit Effect
	///
private:
	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactParticles;

	UPROPERTY(EditAnywhere)
	USoundCue* ImpactSound;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Damage
	///
protected:
	UPROPERTY(EditAnywhere)
	float Damage = 20.f;

	////////////////////////////////////////////////////////////////////////////////////////
};
