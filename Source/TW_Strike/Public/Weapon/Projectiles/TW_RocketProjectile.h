#pragma once

#include "CoreMinimal.h"
#include "Weapon/Projectiles/TW_BaseProjectile.h"
#include "TW_RocketProjectile.generated.h"

UCLASS()
class TW_STRIKE_API ATW_RocketProjectile : public ATW_BaseProjectile
{
	GENERATED_BODY()

public:
	ATW_RocketProjectile();

protected:
	virtual void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float DamageInnerRadius = 200.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float DamageOuterRadius = 500.0f;
	
private:
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* RocketMesh;
	
};
