#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TW_WeaponCore.h"
#include "Interfaces/TW_InteractInterface.h"
#include "TW_BaseWeapon.generated.h"

class ATW_PlayerController;
class ATW_BaseCharacter;
class UTW_AmmoComponent;
class UTW_CrosshairComponent;
class ATW_Casing;
class UTW_PickupComponent;
class UWidgetComponent;
class UAnimationAsset;
class UTexture2D;
class USoundCue;

UCLASS()
class TW_STRIKE_API ATW_BaseWeapon : public AActor, public ITW_InteractInterface
{
	GENERATED_BODY()
	
public:	
	ATW_BaseWeapon();
	virtual void Tick(float DeltaTime) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	FORCEINLINE USkeletalMeshComponent* GetWeaponMesh() const { return WeaponMesh; }
	void DroppedWeapon();
		
	virtual void OnRep_Owner() override;
	void FireEffects(const FVector_NetQuantize& TraceHitTarget);

	void SetHUDAmmo(int32 Ammo);

protected:
	virtual void BeginPlay() override;

	////////////////////////////////////////////////////////////////////////////////////////
	///  Weapon Components
	///
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Components)
	UTW_PickupComponent* PickupComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Components)
	UTW_CrosshairComponent* CrosshairComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Components)
	UTW_AmmoComponent* AmmoComponent;
	
	////////////////////////////////////////////////////////////////////////////////////////
	///  Weapon Mesh
	///
private:
	UPROPERTY(VisibleAnywhere, Category = WeaponProperties)
	USkeletalMeshComponent* WeaponMesh;
	

	////////////////////////////////////////////////////////////////////////////////////////
	///  Weapon State
	///
public:
	FORCEINLINE void SetWeaponState(const EWeaponState State);
	
private:
	UPROPERTY(ReplicatedUsing=OnRep_WeaponState, VisibleAnywhere,  Category = "Weapon Properties")
	EWeaponState WeaponState;

	UFUNCTION()
	void OnRep_WeaponState();

	////////////////////////////////////////////////////////////////////////////////////////
	/// UI
	///
public:
	UPROPERTY(VisibleAnywhere, Category = Setup)
	UWidgetComponent* PickupWidget;

	virtual void SetPickupWidgetVisibility_Implementation(bool bVisible) override;
	////////////////////////////////////////////////////////////////////////////////////////
	/// Fire
	///
public:
	virtual void Fire(const FVector& HitTarget);

	UPROPERTY(EditAnywhere, Category = Combat)
	float FireDelay = .15f;

	UPROPERTY(EditAnywhere, Category = Combat)
	bool bAutomatic = true;
	
protected:
	UPROPERTY(EditAnywhere, Category = "Weapon Properties")
	UAnimationAsset* FireAnimation;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ATW_Casing> CasingClass;
	
	////////////////////////////////////////////////////////////////////////////////////////
	///
	///
public:
	FORCEINLINE EWeaponType GetWeaponType() const { return WeaponType; }

	UPROPERTY(EditAnywhere)
	USoundCue* EquipSound;
	
private:
	ATW_PlayerController* GetPlayerControllerIsValid() const;

	UPROPERTY(EditAnywhere)
	EWeaponType WeaponType;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Zoom 
	///
public:
	UPROPERTY(EditAnywhere)
	float ZoomedFOV = 30.f;

	UPROPERTY(EditAnywhere)
	float ZoomInterpSpeed = 20.f;
};
