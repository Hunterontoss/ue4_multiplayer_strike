#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HUD/TW_CrosshairData.h"
#include "TW_CrosshairComponent.generated.h"


class ATW_BaseCharacter;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_STRIKE_API UTW_CrosshairComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_CrosshairComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
protected:
	virtual void BeginPlay() override;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Crosshair Texture 
	///
public:
	FHUDCrosshairs CalculateCrosshairData(float DeltaTime, ATW_BaseCharacter* PlayerRef);
	
	UPROPERTY(EditAnywhere, Category = Crosshairs)
	UTexture2D* CrosshairsCenter;

	UPROPERTY(EditAnywhere, Category = Crosshairs)
	UTexture2D* CrosshairsLeft;

	UPROPERTY(EditAnywhere, Category = Crosshairs)
	UTexture2D* CrosshairsRight;

	UPROPERTY(EditAnywhere, Category = Crosshairs)
	UTexture2D* CrosshairsTop;

	UPROPERTY(EditAnywhere, Category = Crosshairs)
	UTexture2D* CrosshairsBottom;
	
	////////////////////////////////////////////////////////////////////////////////////////
	/// Crosshair Factor 
	///
	
protected:
	void CalculateCrosshairSpread(float DeltaTime, ATW_BaseCharacter* CharRef);
	FHUDCrosshairs MakeCrosshairData() const;
	
private:
	float CrosshairVelocityFactor = 0.f;
	float CrosshairInAirFactor = 0.f;
	float CrosshairAimFactor = 0.f;
	float CrosshairShootingFactor = 0.75f;
	////////////////////////////////////////////////////////////////////////////////////////
};
