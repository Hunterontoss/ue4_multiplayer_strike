#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_AmmoComponent.generated.h"


class ATW_BaseWeapon;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_STRIKE_API UTW_AmmoComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_AmmoComponent();

protected:
	virtual void BeginPlay() override;

public:
	void SpendRound();
	FORCEINLINE int32 GetAmmo() const { return Ammo; }
	void TryAddAmmo(int32 AmmoToAdd);
	FORCEINLINE int32 GetMagCapacity() const { return MagCapacity; }
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	bool IsEmpty() const;
	
protected:
	UFUNCTION()
	void OnRep_Ammo();
	
	UPROPERTY(EditAnywhere, ReplicatedUsing=OnRep_Ammo)
	int32 Ammo;
	
	UPROPERTY(EditAnywhere)
	int32 MagCapacity;

private:
	UPROPERTY()
	ATW_BaseWeapon* ComponentOwner;

};
