
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_StrikeTypes/CombatState.h"
#include "Weapon/TW_WeaponCore.h"
#include "TW_CombatComponent.generated.h"

class UTW_ZoomCameraComponent;
class ATW_PlayerHUD;
class ATW_PlayerController;
class UTW_TraceComponent;
class ATW_BaseWeapon;
class ATW_BaseCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_STRIKE_API UTW_CombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_CombatComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void HideWeaponMesh(bool bNeedHide) const;

	friend ATW_BaseCharacter;
	
	void EquipWeapon(ATW_BaseWeapon* WeaponToEquip);
	void DropWeapon();
	
protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	ATW_BaseCharacter* Character;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Weapon Equip
	///
	
public:
	UFUNCTION()
	void OnRep_EquippedWeapon();

	ATW_BaseWeapon* GetEquippedWeapon() const { return EquippedWeapon; }
	
private:
	UPROPERTY(ReplicatedUsing=OnRep_EquippedWeapon)
	ATW_BaseWeapon* EquippedWeapon;

	void PlayEquipSound() const;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Aiming
	///
public:
	void SetAiming(bool bIsAiming);

	UPROPERTY(EditAnywhere)
	float BaseWalkSpeed = 600.f;

	UPROPERTY(EditAnywhere)
	float AimWalkSpeed = 400.f;
	
protected:
	UFUNCTION(Server, Reliable)
	void ServerSetAiming(bool bIsAiming);

private:
	UPROPERTY(Replicated)
	bool bAiming;
	
	void ChangeSpeedWhenAim();

	////////////////////////////////////////////////////////////////////////////////////////
	/// Fire
	///
public:
	bool CanFire() const;
	void FireButtonPressed(bool bIspPressed);
	
protected:
	UFUNCTION(Server, Reliable)
	void ServerFire(const FVector_NetQuantize& TraceHitTarget);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastFire(const FVector_NetQuantize& TraceHitTarget);

	UFUNCTION(Client, Reliable)
	void ClientFire();
	
private:
	bool bFireButtonPressed;

	UPROPERTY()
	UTW_TraceComponent* TraceComp;

	UPROPERTY()
	UTW_ZoomCameraComponent* ZoomComp;
	////////////////////////////////////////////////////////////////////////////////////////
	///
	///

protected:
	void SetHUDCrosshairs(float DeltaTime, FHitResult HitResult);
	
private:
	bool CheckHUDIsValid();

	UPROPERTY()
	ATW_PlayerController* Controller;

	UPROPERTY()
	ATW_PlayerHUD* HUD;
	
	FVector HitTarget;

	////////////////////////////////////////////////////////////////////////////////////////
	/// FIRE  
	///
protected:
	void Fire();

private:
	void StartFireTimer();
	void FinishedFireTimer();
	
	FTimerHandle FireTimer_Handle;
	
	bool bCanFire = true;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Ammo
	///
protected:
	void InitializeCarriedAmmo();
	
private:
	// Carried ammo for the currently-equipped weapon
	UPROPERTY(ReplicatedUsing = OnRep_CarriedAmmo)
	int32 CarriedAmmo;

	UFUNCTION()
	void OnRep_CarriedAmmo();

	TMap<EWeaponType, int32> CarriedAmmoMap;

	UPROPERTY(EditAnywhere)
	int32 StartingARAmmo = 90;

	UPROPERTY(EditAnywhere)
	int32 StartingPistolAmmo = 30;

	UPROPERTY(EditAnywhere)
	int32 StartingRocketLauncherAmmo = 4;

	UPROPERTY(EditAnywhere)
	int32 StartingSniperRifleAmmo = 12;
	
	////////////////////////////////////////////////////////////////////////////////////////
	/// Reload
	///
	
public:
	void ReloadCurrentWeapon();

	UFUNCTION(BlueprintCallable)
	void FinishReloading();
	
protected:
	UFUNCTION(Server, Reliable)
	void ServerReload();

	void HandleReload() const;

	int32 AmountToReload();

private:
	void UpdateAmmoValues();
	
	////////////////////////////////////////////////////////////////////////////////////////
	/// Combat State 
	///

public:
	ECombatState GetCombatState() const;
	
private:
	UPROPERTY(ReplicatedUsing = OnRep_CombatState)
	ECombatState CombatState = ECombatState::ECS_Unoccupied;

	UFUNCTION()
	void OnRep_CombatState();

	////////////////////////////////////////////////////////////////////////////////////////
};
