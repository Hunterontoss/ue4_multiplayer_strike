#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "TW_GameState.generated.h"

class ATW_PlayerState;

UCLASS()
class TW_STRIKE_API ATW_GameState : public AGameState
{
	GENERATED_BODY()

public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	void UpdateTopScore(ATW_PlayerState* ScoringPlayer);

	UPROPERTY(Replicated)
	TArray<ATW_PlayerState*> TopScoringPlayers;

private:
	float TopScore = 0.f;
	
};
