#pragma once
#include "TW_CrosshairData.generated.h"

class UTexture2D;

USTRUCT(BlueprintType)
struct FHUDCrosshairs
{
	GENERATED_BODY()
	
	UTexture2D* CrosshairsCenter;
	UTexture2D* CrosshairsLeft;
	UTexture2D* CrosshairsRight;
	UTexture2D* CrosshairsTop;
	UTexture2D* CrosshairsBottom;
	float CrosshairSpread;
	FLinearColor CrosshairsColor;
};