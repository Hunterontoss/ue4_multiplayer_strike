#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TW_OverheadWidget.generated.h"

/**
 * 
 */
UCLASS()
class TW_STRIKE_API UTW_OverheadWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* DisplayText;

	void SetDisplayText(FString TextToDisplay);

	UFUNCTION(BlueprintCallable)
	void ShowPlayerNetRole(APawn* InPawn);
protected:
	virtual void NativeDestruct() override;
private:
	FString GetRoleName(const ENetRole Role);
};
