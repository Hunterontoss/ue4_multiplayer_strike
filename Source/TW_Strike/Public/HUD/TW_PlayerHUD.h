#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "HUD/TW_CrosshairData.h"
#include "TW_PlayerHUD.generated.h"

class UTW_MatchWaitingOverlay;
class UTexture2D;
class UUserWidget;
class UTW_PlayerOverlay;

UCLASS()
class TW_STRIKE_API ATW_PlayerHUD : public AHUD
{
	GENERATED_BODY()

public:
	virtual void DrawHUD() override;

	FORCEINLINE void SetHUDPackage(const FHUDCrosshairs& Package) { HUDCrosshairs = Package; }

protected:
	virtual void BeginPlay() override;
	
private:
	void DrawCrosshair(UTexture2D* Texture, FVector2D ViewportCenter, FVector2D Spread, FLinearColor CrosshairColor);
	
	FHUDCrosshairs HUDCrosshairs;

	UPROPERTY(EditAnywhere)
	float CrosshairSpreadMax = 16.f;

	////////////////////////////////////////////////////////////////////////////////////////
	///  Player Overlay 
	///
public:
	UPROPERTY(EditAnywhere, Category = "Player Stats")
	TSubclassOf<UUserWidget> CharacterOverlayClass;

	UPROPERTY()
	UTW_PlayerOverlay* CharacterOverlay;
	
	void AddCharacterOverlay();
	
	////////////////////////////////////////////////////////////////////////////////////////
	/// Match Waiting Overlay
	///
public:
	UPROPERTY(EditAnywhere, Category = "MatchState")
	TSubclassOf<UUserWidget> MatchWaitingClass;

	UPROPERTY()
	UTW_MatchWaitingOverlay* MatchWaitingOverlay;

	void AddMatchWaitingOverlay();
};
