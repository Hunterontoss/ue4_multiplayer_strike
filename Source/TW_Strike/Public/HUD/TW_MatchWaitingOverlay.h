#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TW_MatchWaitingOverlay.generated.h"

class UTextBlock;
/**
 * 
 */
UCLASS()
class TW_STRIKE_API UTW_MatchWaitingOverlay : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* WarmupTime;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* AnnouncementText;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* InfoText;
};
