#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "TW_StrikeTypes/TurningInPlace.h"
#include "TW_BaseAnimInstance.generated.h"

class ATW_BaseCharacter;
UCLASS()
class TW_STRIKE_API UTW_BaseAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	virtual void NativeInitializeAnimation() override;
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Flags
	///
protected:
	UPROPERTY(BlueprintReadOnly, Category = Character)
	ATW_BaseCharacter* BaseCharacter;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	float CurrentSpeed;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	bool bIsInAir;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	bool bIsAccelerating;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	bool bWeaponEquipped;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	bool bIsCrouched;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	bool bAiming;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	FTransform LeftHandTransform;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	ETurningInPlace TurningInPlace;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	FRotator RightHandRotation;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	bool bLocallyControlled;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	bool bRotateRootBone;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	bool bEliminated;

	UPROPERTY(BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bUseFABRIK;

	// When Reloading don't need it
	UPROPERTY(BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bUseAimOffsets;

	UPROPERTY(BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bTransformRightHand;
	
private:
	void UpdateVariables(float DeltaSeconds);
	////////////////////////////////////////////////////////////////////////////////////////
	/// Lean 
	///
protected:
	UPROPERTY(BlueprintReadOnly, Category = Movement)
	float YawOffset = 0.f;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	float Lean = 0.f;

	FRotator CharacterRotationLastFrame;
	FRotator CharacterRotation;
	FRotator DeltaRotation;
private:
	void UpdateLean(float DeltaSeconds);
	////////////////////////////////////////////////////////////////////////////////////////
	/// Aim Weapon Offset
	///
protected:
	UPROPERTY(BlueprintReadOnly, Category = Movement)
	float AO_Yaw;

	UPROPERTY(BlueprintReadOnly, Category = Movement)
	float AO_Pitch;
	
	////////////////////////////////////////////////////////////////////////////////////////
	///
	///
private:
	bool CheckWeaponMesh();
	
};
