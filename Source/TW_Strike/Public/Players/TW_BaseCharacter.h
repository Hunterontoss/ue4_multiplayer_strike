#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Interfaces/TW_InteractWithCroshairInterface.h"
#include "Weapon/TW_WeaponCore.h"
#include "TW_BaseCharacter.generated.h"

class ATW_PlayerState;
class ATW_PlayerController;
class UTW_HealthComponent;
class UTW_ZoomCameraComponent;
class UTW_TraceComponent;
class UTW_AimOffsetCharacter;
class ATW_BaseWeapon;
class UTW_CombatComponent;
class USpringArmComponent;
class UCameraComponent;
class UAnimMontage;

UCLASS()
class TW_STRIKE_API ATW_BaseCharacter : public ACharacter, public ITW_InteractWithCroshairInterface
{
	GENERATED_BODY()

public:
	ATW_BaseCharacter();
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void PostInitializeComponents() override;
	void UpdateHUDHealth(const float Health, const float MaxHealth);
	
protected:
	virtual void BeginPlay() override;
	virtual void Jump() override;
	
	
	////////////////////////////////////////////////////////////////////////////////////////
	/// Weapon
	///
public:
	FORCEINLINE void SetOverlappingWeapon(ATW_BaseWeapon* NewWeapon) { OverlappingWeapon= NewWeapon;}
	bool IsWeaponEquipped() const;
	bool IsAiming();
	
private:
	UPROPERTY()
	ATW_BaseWeapon* OverlappingWeapon;
	
	////////////////////////////////////////////////////////////////////////////////////////
	/// Components
	///
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Components)
	UTW_CombatComponent* CombatComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Components)
	UTW_HealthComponent* HealthComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Components)
	UTW_AimOffsetCharacter* AimOffsetCharacterComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Components)
	UTW_TraceComponent* TraceComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Components)
	UTW_ZoomCameraComponent* ZoomComponent;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Camera 
	///
public:
	UPROPERTY(VisibleAnywhere, Category = Camera)
	USpringArmComponent* CameraBoom;
	
	UPROPERTY(VisibleAnywhere, Category = Camera)
	UCameraComponent* FollowCamera;
	
	////////////////////////////////////////////////////////////////////////////////////////
	/// Input 
	///

protected:
	void MoveForward(float Value);
	void MoveRight(float Value);
	void Turn(float Value);
	void LookUp(float Value);
	void CrouchButtonPressed();
	
	void EquipButtonPressed();

	void AimButtonPressed();
	void AimButtonReleased();

	void FireButtonPressed();
	void FireButtonReleased();

	void ReloadButtonPressed();
	
	UFUNCTION(Server, Reliable)
	void ServerEquipButtonPressed();

	////////////////////////////////////////////////////////////////////////////////////////
	/// Widgets
	///
	
private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UWidgetComponent* OverheadWidget;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Fire Montage
	///
public:
	void PlayFireMontage(bool bAiming);

private:
	UPROPERTY(EditAnywhere, Category = Combat)
	UAnimMontage* FireWeaponMontage;

	////////////////////////////////////////////////////////////////////////////////////////
	/// HitReact 
	///
public:
	void PlayHitReactMontage();
	
private:
	UPROPERTY(EditAnywhere, Category = Combat)
	UAnimMontage* HitReactMontage;
	
	////////////////////////////////////////////////////////////////////////////////////////
	///
	
public:
	FVector GetHitTarget() const;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Hide Character if Camera nearby
	/// 
private:
	void HideCameraIfCharacterClose();

	UPROPERTY(EditAnywhere)
	float CameraThreshold = 100.f;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Fix Proxy Turn Sync
	///

public:
	virtual void OnRep_ReplicatedMovement() override;
private:
	float TimeSinceLastMovementReplication;

private:
	UPROPERTY()
	ATW_PlayerController* PlayerController;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Eliminate 
	///
public:
	void Eliminate();
	
	UFUNCTION(NetMulticast, Reliable)
	void MulticastEliminatePlayer();

	void PlayEliminateMontage();
	
	FORCEINLINE bool IsEliminated() const { return bEliminated; }

	// TODO: Disable When Restart Cooldown
	UPROPERTY(Replicated)
	bool bDisableGameplay = false;
protected:
	UPROPERTY(EditAnywhere, Category = Combat)
	UAnimMontage* EliminateMontage;
	
private:
	bool bEliminated = false;

	FTimerHandle EliminateTimer_Handle;

	UPROPERTY(EditDefaultsOnly)
	float EliminateDelay = 3.f;

	void EliminateTimerFinished();

	////////////////////////////////////////////////////////////////////////////////////////
	/// Score 
	///

	// On First Frame Player State is Null, Need wait when Player State Is Init
	void ScoreStateInit();
	
	UPROPERTY()
	ATW_PlayerState* CharacterPlayerState;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Reload
	///

public:
	void PlayReloadMontage(const EWeaponType WeaponType) const;
	
protected:
	UPROPERTY(EditAnywhere, Category = Combat)
	UAnimMontage* ReloadMontage;
};
