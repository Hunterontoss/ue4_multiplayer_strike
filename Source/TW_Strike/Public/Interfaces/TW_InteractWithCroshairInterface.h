#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TW_InteractWithCroshairInterface.generated.h"

UINTERFACE(MinimalAPI)
class UTW_InteractWithCroshairInterface : public UInterface
{
	GENERATED_BODY()
};

class TW_STRIKE_API ITW_InteractWithCroshairInterface
{
	GENERATED_BODY()
	
public:
};
