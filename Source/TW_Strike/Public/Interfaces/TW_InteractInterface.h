// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TW_InteractInterface.generated.h"
UINTERFACE(MinimalAPI)
class UTW_InteractInterface : public UInterface
{
	GENERATED_BODY()
	
};

class TW_STRIKE_API ITW_InteractInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SetPickupWidgetVisibility(bool bVisible);
};
