#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "PlayerController/TW_PlayerController.h"
#include "TW_StrikeGameMode.generated.h"

class ATW_BaseCharacter;
class ATW_PlayerController;

namespace MatchState
{
	extern TW_STRIKE_API const FName Cooldown; // Match duration has been reached. Display winner and begin cooldown timer.
}

UCLASS()
class TW_STRIKE_API ATW_StrikeGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATW_StrikeGameMode();
	
	virtual void Tick(float DeltaSeconds) override;
	
protected:
	virtual void BeginPlay() override;
	
public:
	virtual void PlayerEliminated(ATW_BaseCharacter* ElimmedCharacter, ATW_PlayerController* VictimController, ATW_PlayerController* AttackerController);

	virtual void RequestRespawn(ACharacter* ElimmedCharacter, AController* ElimmedController);

private:
	void UpdatePlayerState(ATW_PlayerController* VictimController, ATW_PlayerController* AttackerController) const;


	////////////////////////////////////////////////////////////////////////////////////////
	/// Время перед началом раунда 
	///
public:
	UPROPERTY(EditDefaultsOnly)
	float WarmupTime = 5.f;

	float LevelStartingTime = 0.f;

	UPROPERTY(EditDefaultsOnly)
	float MatchTime = 120.f;

	UPROPERTY(EditDefaultsOnly)
	float CooldownTime = 6.f;
	
private:
	float CountdownTime = 0.f;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Match State  
	///
protected:
	virtual void OnMatchStateSet() override;

	////////////////////////////////////////////////////////////////////////////////////////
};
