#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TW_PlayerController.generated.h"

class UTW_PlayerOverlay;
class ATW_PlayerHUD;

UCLASS()
class TW_STRIKE_API ATW_PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	////////////////////////////////////////////////////////////////////////////////////////
	/// HUD
	///
	void SetHUDHealth(float Health, float MaxHealth);
	void SetHUDScore(float Score);
	void SetHUDDefeats(int32 Defeats);
	void SetHUDWeaponAmmo(int32 Ammo);
	void SetHUDCarriedAmmo(int32 Ammo);
	void SetHUDMatchCountdown(float CountdownTime);
	void SetHUDWaitingMatch(float CountdownTime);

private:
	bool IsHUD_Valid();
	////////////////////////////////////////////////////////////////////////////////////////
	///
	///
	
protected:
	virtual void BeginPlay() override;

	void SetHUDTime();
protected:
	UFUNCTION(Server, Reliable)
	void ServerCheckMatchState();

	UFUNCTION(Client, Reliable)
	void ClientJoinMidgame(FName StateOfMatch, float Warmup, float Match, float Cooldown, float StartingTime);
	
private:
	UPROPERTY()
	ATW_PlayerHUD* PlayerHUD;

	float MatchTime = 0.f;
	float LevelStartingTime = 0.f;
	float CooldownTime = 0.f;
	float WarmupTime = 0.f;
	uint32 CountdownInt = 0;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Sync Server Time
	///

public:
	// Получить синхронизированное время с мировым временем сервера
	virtual float GetServerTime();

	// Начать синхронизацию с серверным временем на стороне клиента, нужно как можно скорее
	virtual void ReceivedPlayer() override; 

protected:
	// Запрашивает текущее время сервера, передавая время клиента при отправке запроса
	UFUNCTION(Server, Reliable)
	void ServerRequestServerTime(float TimeOfClientRequest);

	// Сообщает текущее время сервера клиенту в ответ на ServerRequestServerTime
	UFUNCTION(Client, Reliable)
	void ClientReportServerTime(float TimeOfClientRequest, float TimeServerReceivedClientRequest);

	// разница между временем клиента и сервера
	float ClientServerDelta = 0.f; 

	// Частота обновления времени
	UPROPERTY(EditAnywhere, Category = Time)
	float TimeSyncFrequency = 5.f;

	float TimeSyncRunningTime = 0.f;
	void CheckTimeSync(float DeltaTime);

	////////////////////////////////////////////////////////////////////////////////////////
	/// Match State 
	///
public:
	void OnMatchStateSet(FName State);
	void HandleMatchHasStarted();
	void HandleCooldown();
	
protected:
	void PollInit();

	UPROPERTY(ReplicatedUsing = OnRep_MatchState)
	FName MatchState;

	UFUNCTION()
	void OnRep_MatchState();

	UPROPERTY()
	UTW_PlayerOverlay* CharacterOverlay;
	bool bInitializeCharacterOverlay = false;

	float HUDHealth;
	float HUDMaxHealth;
	float HUDScore;
	int32 HUDDefeats;

	
	////////////////////////////////////////////////////////////////////////////////////////
	/// 
	///
};
