#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "TW_PlayerState.generated.h"

class ATW_BaseCharacter;
class ATW_PlayerController;

UCLASS()
class TW_STRIKE_API ATW_PlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void OnRep_Score() override;

	UFUNCTION()
	virtual void OnRep_Defeats();

	void AddToScore(float ScoreAmount);
	void AddToDefeats(int32 DefeatsAmount);
	FORCEINLINE int32 GetDefeats() const { return Defeats;}
	
private:
	UPROPERTY(ReplicatedUsing = OnRep_Defeats)
	int32 Defeats;

	ATW_PlayerController* GetPlayerControllerIsValid() const;
	
};
