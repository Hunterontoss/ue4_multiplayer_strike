#include "Players/TW_BaseAnimInstance.h"
#include "Components/TW_AimOffsetCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Players/TW_BaseCharacter.h"
#include "Weapon/TW_BaseWeapon.h"
#include "Weapon/Components/TW_CombatComponent.h"

void UTW_BaseAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	BaseCharacter = Cast<ATW_BaseCharacter>(TryGetPawnOwner());
}

void UTW_BaseAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	UpdateVariables(DeltaSeconds);
}

void UTW_BaseAnimInstance::UpdateVariables(float DeltaSeconds)
{
	if (!BaseCharacter)
	{
		BaseCharacter = Cast<ATW_BaseCharacter>(TryGetPawnOwner());
	}
	if (!BaseCharacter) return;

	FVector Velocity = BaseCharacter->GetVelocity();
	Velocity.Z = 0.f;
	CurrentSpeed = Velocity.Size();

	bIsInAir = BaseCharacter->GetCharacterMovement()->IsFalling();
	bIsAccelerating = BaseCharacter->GetCharacterMovement()->GetCurrentAcceleration().Size() > 0.f ? true : false;
	bWeaponEquipped = BaseCharacter->IsWeaponEquipped();
	bIsCrouched = BaseCharacter->bIsCrouched;
	bAiming = BaseCharacter->IsAiming();

	UpdateLean(DeltaSeconds);

	// Aim Offset 
	AO_Yaw = BaseCharacter->AimOffsetCharacterComponent->GetAO_Yaw();
	AO_Pitch = BaseCharacter->AimOffsetCharacterComponent->GetAO_Pitch();
	TurningInPlace = BaseCharacter->AimOffsetCharacterComponent->GetTurnInPlace();

	// Sync Proxy Player Rotation Bone
	bRotateRootBone = BaseCharacter->AimOffsetCharacterComponent->ShouldRotateRootBone();
	bEliminated = BaseCharacter->IsEliminated();

	// FABRIK Left Hand on Weapon
	if (CheckWeaponMesh())
	{
		const auto* WeaponMesh = BaseCharacter->CombatComponent->GetEquippedWeapon()->GetWeaponMesh();
		LeftHandTransform = WeaponMesh->GetSocketTransform(FName("LeftHandSocket"), ERelativeTransformSpace::RTS_World);
		
		FVector OutPosition;
		FRotator OutRotation;
		
		BaseCharacter->GetMesh()->TransformToBoneSpace(FName("hand_r"), LeftHandTransform.GetLocation(), FRotator::ZeroRotator, OutPosition, OutRotation);
		LeftHandTransform.SetLocation(OutPosition);
		LeftHandTransform.SetRotation(FQuat(OutRotation));

		if (BaseCharacter->IsLocallyControlled())
		{
			bLocallyControlled = true;
			const FTransform L_RightHandTransform = BaseCharacter->CombatComponent->GetEquippedWeapon()->GetWeaponMesh()->GetSocketTransform(FName("hand_r"), ERelativeTransformSpace::RTS_World);

			const FVector Target =
				L_RightHandTransform.GetLocation() + (L_RightHandTransform.GetLocation() - BaseCharacter->GetHitTarget());
			
			const FRotator L_LookAtRotation =
				UKismetMathLibrary::FindLookAtRotation(L_RightHandTransform.GetLocation(), Target);

			RightHandRotation = FMath::RInterpTo(RightHandRotation, L_LookAtRotation, DeltaSeconds, 30.f);
		}
	}
	
	bUseFABRIK = BaseCharacter->CombatComponent->GetCombatState() != ECombatState::ECS_Reloading;
	bUseAimOffsets = BaseCharacter->CombatComponent->GetCombatState() != ECombatState::ECS_Reloading;
	bTransformRightHand = BaseCharacter->CombatComponent->GetCombatState() != ECombatState::ECS_Reloading;
}

void UTW_BaseAnimInstance::UpdateLean(float DeltaSeconds)
{
	// Offset Yaw for Strafing
	const FRotator AimRotation = BaseCharacter->GetBaseAimRotation();
	const FRotator MovementRotation = UKismetMathLibrary::MakeRotFromX(BaseCharacter->GetVelocity());
	YawOffset = UKismetMathLibrary::NormalizedDeltaRotator(MovementRotation, AimRotation).Yaw;

	// Smooth Rotation for Animation
	const FRotator DeltaRot = UKismetMathLibrary::NormalizedDeltaRotator(MovementRotation, AimRotation);
	DeltaRotation = FMath::RInterpTo(DeltaRotation, DeltaRot, DeltaSeconds, 6.f);
	YawOffset = DeltaRotation.Yaw;
    
	//// UNLOCK if Need Lean 
	//CharacterRotationLastFrame = CharacterRotation;
	//CharacterRotation = BaseCharacter->GetActorRotation();
	//const FRotator Delta = UKismetMathLibrary::NormalizedDeltaRotator(CharacterRotation, CharacterRotationLastFrame);
	//const float Target = Delta.Yaw / DeltaSeconds;
	//const float Interp = FMath::FInterpTo(Lean, Target, DeltaSeconds, 6.f);
	//Lean = FMath::Clamp(Interp, -90.f, 90.f);
	////
}

bool UTW_BaseAnimInstance::CheckWeaponMesh()
{
	return	bWeaponEquipped &&
			BaseCharacter->GetMesh() &&
			BaseCharacter->CombatComponent->GetEquippedWeapon() &&
			BaseCharacter->CombatComponent->GetEquippedWeapon()->GetWeaponMesh();
}
