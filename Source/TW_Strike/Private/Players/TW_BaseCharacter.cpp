#include "Players/TW_BaseCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/TW_AimOffsetCharacter.h"
#include "Components/TW_HealthComponent.h"
#include "Components/TW_TraceComponent.h"
#include "Components/TW_ZoomCameraComponent.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameModes/TW_StrikeGameMode.h"
#include "PlayerController/TW_PlayerController.h"
#include "PlayerState/TW_PlayerState.h"
#include "TW_Strike/TW_Strike.h"
#include "Weapon/Components/TW_CombatComponent.h"
#include "Weapon/TW_WeaponCore.h"

ATW_BaseCharacter::ATW_BaseCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	//SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	
	HealthComponent = CreateDefaultSubobject<UTW_HealthComponent>(TEXT("HealthComp"));
	HealthComponent->SetIsReplicated(true);
	
	////////////////////////////////////////////////////////////////////////////////////////
	/// Camera 
	///
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetMesh());
	CameraBoom->TargetArmLength = 250.f;
	CameraBoom->bUsePawnControlRotation = true;
	CameraBoom->SocketOffset = FVector(0.f, 60.f, 80.f);

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;

	bUseControllerRotationYaw = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	
	////////////////////////////////////////////////////////////////////////////////////////
	///
	///
	OverheadWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("OverheadWidget"));
	OverheadWidget->SetupAttachment(RootComponent);

	CombatComponent = CreateDefaultSubobject<UTW_CombatComponent>(TEXT("CombatComp"));
	CombatComponent->SetIsReplicated(true);

	GetCharacterMovement()->NavAgentProps.bCanCrouch = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 850.f, 0.f);

	////////////////////////////////////////////////////////////////////////////////////////
	/// Pawn Ignore Camera Chanel Cuz Enemy block Camera and this annoying  
	///
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);

	////////////////////////////////////////////////////////////////////////////////////////
	/// Turning 
	///
	AimOffsetCharacterComponent = CreateDefaultSubobject<UTW_AimOffsetCharacter>(TEXT("AimOffsetComp"));
	AimOffsetCharacterComponent->SetIsReplicated(false);
	////////////////////////////////////////////////////////////////////////////////////////
	/// NetWorking 
	///
	NetUpdateFrequency = 66.f;
	MinNetUpdateFrequency = 33.f;
	
	////////////////////////////////////////////////////////////////////////////////////////
	///
	///

	TraceComponent = CreateDefaultSubobject<UTW_TraceComponent>(TEXT("TraceComp"));
	
	ZoomComponent = CreateDefaultSubobject<UTW_ZoomCameraComponent>(TEXT("ZoomComp"));
	// FollowCamera -> Lens -> Aperture (F-Stop); if need sharpness when Zoomed

	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	GetMesh()->SetCollisionObjectType(ECC_SKELETAL_MESH);
	
}

void ATW_BaseCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void ATW_BaseCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (CombatComponent)
	{
		CombatComponent->Character = this;
	}

	if (AimOffsetCharacterComponent)
	{
		AimOffsetCharacterComponent->Character = this;
	}
}


void ATW_BaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	const float Health = HealthComponent->GetHealth();
	const float MaxHealth = HealthComponent->GetMaxHealth();
	UpdateHUDHealth(Health, MaxHealth);
	if (PlayerController)
	{
		PlayerController->SetHUDWeaponAmmo(0);
	}
}

void ATW_BaseCharacter::UpdateHUDHealth(const float Health, const float MaxHealth)
{
	PlayerController = PlayerController == nullptr ? Cast<ATW_PlayerController>(Controller) : PlayerController;

	if (PlayerController)
	{
		PlayerController->SetHUDHealth(Health, MaxHealth);
	}
}

void ATW_BaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetLocalRole() > ENetRole::ROLE_SimulatedProxy && IsLocallyControlled())
	{
		AimOffsetCharacterComponent->AimOffset(DeltaTime);	
	}
	else
	{
		// Proxy Sync
		TimeSinceLastMovementReplication += DeltaTime;
		if (TimeSinceLastMovementReplication > 0.25f)
		{
			OnRep_ReplicatedMovement();
		}
		AimOffsetCharacterComponent->CalculateAO_Pitch();
	}

	HideCameraIfCharacterClose();
	// TODO: Make Better
	ScoreStateInit();
}

void ATW_BaseCharacter::OnRep_ReplicatedMovement()
{
	Super::OnRep_ReplicatedMovement();

	AimOffsetCharacterComponent->SimProxiesTurn();
	TimeSinceLastMovementReplication = 0.f;
}

void ATW_BaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ATW_BaseCharacter::Jump);
	PlayerInputComponent->BindAction("Equip", IE_Pressed, this, &ATW_BaseCharacter::EquipButtonPressed);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ATW_BaseCharacter::CrouchButtonPressed);

	PlayerInputComponent->BindAction("Aiming", IE_Pressed, this, &ATW_BaseCharacter::AimButtonPressed);
	PlayerInputComponent->BindAction("Aiming", IE_Released, this, &ATW_BaseCharacter::AimButtonReleased);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ATW_BaseCharacter::FireButtonPressed);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ATW_BaseCharacter::FireButtonReleased);

	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ATW_BaseCharacter::ReloadButtonPressed);
	
	PlayerInputComponent->BindAxis("MoveForward", this, &ATW_BaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATW_BaseCharacter::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &ATW_BaseCharacter::Turn);
	PlayerInputComponent->BindAxis("LookUp", this, &ATW_BaseCharacter::LookUp);
}

void ATW_BaseCharacter::Jump()
{
	if (bIsCrouched)
	{
		UnCrouch();
	}
	else
	{
		Super::Jump();
	}
}

bool ATW_BaseCharacter::IsWeaponEquipped() const
{
	return  CombatComponent && CombatComponent->EquippedWeapon;
}

void ATW_BaseCharacter::MoveForward(float Value)
{
	if (Controller != nullptr && Value != 0.f)
	{
		const FRotator YawRotation(0.f, Controller->GetControlRotation().Yaw, 0.f);
		const FVector Direction(FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X));
		AddMovementInput(Direction, Value);
	}
}

void ATW_BaseCharacter::MoveRight(float Value)
{
	if (Controller != nullptr && Value != 0.f)
	{
		const FRotator YawRotation(0.f, Controller->GetControlRotation().Yaw, 0.f);
		const FVector Direction(FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y));
		AddMovementInput(Direction, Value);
	}
}

void ATW_BaseCharacter::Turn(float Value)
{
	AddControllerYawInput(Value);
}

void ATW_BaseCharacter::LookUp(float Value)
{
	AddControllerPitchInput(Value);
}

void ATW_BaseCharacter::CrouchButtonPressed()
{
	if (bIsCrouched)
	{
		UnCrouch();
	}
	else
	{
		Crouch();
	}
}

void ATW_BaseCharacter::EquipButtonPressed()
{
	if (!OverlappingWeapon) return;

	
	if (CombatComponent)
	{
		if (HasAuthority())
		{
			CombatComponent->EquipWeapon(OverlappingWeapon);	
		}
		else
		{
			ServerEquipButtonPressed();
		}
	}
}

void ATW_BaseCharacter::AimButtonPressed()
{
	if (CombatComponent)
	{
		CombatComponent->SetAiming(true);
	}
}

void ATW_BaseCharacter::AimButtonReleased()
{
	if (CombatComponent)
	{
		CombatComponent->SetAiming(false);
	}
}

void ATW_BaseCharacter::FireButtonPressed()
{
	if (CombatComponent)
	{
		CombatComponent->FireButtonPressed(true);
	}
}

void ATW_BaseCharacter::FireButtonReleased()
{
	if (CombatComponent)
	{
		CombatComponent->FireButtonPressed(false);
	}
}

void ATW_BaseCharacter::ReloadButtonPressed()
{
	if (CombatComponent)
	{
		CombatComponent->ReloadCurrentWeapon();
	}
}

void ATW_BaseCharacter::PlayFireMontage(bool bAiming)
{
	if (!CombatComponent || !CombatComponent->EquippedWeapon) return;

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && FireWeaponMontage)
	{
		AnimInstance->Montage_Play(FireWeaponMontage);
		const FName SectionName = bAiming ? FName("RifleAim") : FName("RifleHip");
		AnimInstance->Montage_JumpToSection(SectionName);
	}
}


void ATW_BaseCharacter::PlayHitReactMontage()
{
	if (CombatComponent == nullptr || CombatComponent->EquippedWeapon == nullptr) return;

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && HitReactMontage)
	{
		AnimInstance->Montage_Play(HitReactMontage);
		const FName SectionName("FromFront");
		AnimInstance->Montage_JumpToSection(SectionName);
	}
}

bool ATW_BaseCharacter::IsAiming()
{
	return (CombatComponent && CombatComponent->bAiming);
}

void ATW_BaseCharacter::ServerEquipButtonPressed_Implementation()
{
	if (!OverlappingWeapon) return;

	if (CombatComponent)
	{
		CombatComponent->EquipWeapon(OverlappingWeapon);
	}
}

FVector ATW_BaseCharacter::GetHitTarget() const
{
	if (CombatComponent)
	{
		return CombatComponent->HitTarget;
	}
	return FVector();
}

void ATW_BaseCharacter::HideCameraIfCharacterClose()
{
	if (!IsLocallyControlled()) return;
	
	if ((FollowCamera->GetComponentLocation() - GetActorLocation()).Size() < CameraThreshold)
	{
		GetMesh()->SetVisibility(false);
		if (CombatComponent && CombatComponent->EquippedWeapon)
		{
			CombatComponent->HideWeaponMesh(true);
		}
	}
	else
	{
		GetMesh()->SetVisibility(true);
		if (CombatComponent && CombatComponent->EquippedWeapon)
		{
			CombatComponent->HideWeaponMesh(false);
		}
	}
}

// Eliminate Section Start 
void ATW_BaseCharacter::MulticastEliminatePlayer_Implementation()
{
	if (PlayerController)
	{
		PlayerController->SetHUDWeaponAmmo(0);
	}
	
	bEliminated = true;
	
	GetCharacterMovement()->DisableMovement();
	GetCharacterMovement()->StopMovementImmediately();
	if (PlayerController)
	{
		DisableInput(PlayerController);
	}
	// Disable collision
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	bDisableGameplay = true;
	if (CombatComponent)
	{
		CombatComponent->FireButtonPressed(false);
	}
	
	PlayEliminateMontage();
}

void ATW_BaseCharacter::Eliminate()
{
	if (CombatComponent)
	{
		CombatComponent->DropWeapon();
	}
	
	MulticastEliminatePlayer();

	GetWorldTimerManager().SetTimer(
		EliminateTimer_Handle,
		this,
		&ATW_BaseCharacter::EliminateTimerFinished,
		EliminateDelay
	);
}

void ATW_BaseCharacter::PlayEliminateMontage()
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && EliminateMontage)
	{
		AnimInstance->Montage_Play(EliminateMontage);
	}
}

void ATW_BaseCharacter::EliminateTimerFinished()
{
	auto* BlasterGameMode = GetWorld()->GetAuthGameMode<ATW_StrikeGameMode>();
	if (BlasterGameMode)
	{
		BlasterGameMode->RequestRespawn(this, Controller);
	}
}

// Eliminate Section End

void ATW_BaseCharacter::ScoreStateInit()
{
	if (CharacterPlayerState == nullptr)
	{
		CharacterPlayerState = GetPlayerState<ATW_PlayerState>();
		if (CharacterPlayerState)
		{
			CharacterPlayerState->AddToScore(0.f);
			CharacterPlayerState->AddToDefeats(0);
		}
	}	
}

void ATW_BaseCharacter::PlayReloadMontage(const EWeaponType WeaponType) const
{
	if (CombatComponent == nullptr || CombatComponent->EquippedWeapon == nullptr) return;

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && ReloadMontage)
	{
		AnimInstance->Montage_Play(ReloadMontage);
		FName SectionName;

		switch (WeaponType)
		{
		case EWeaponType::EWT_AssaultRifle:
			SectionName = FName("Rifle");
			break;
		case EWeaponType::EWT_SniperRifle:
			SectionName = FName("Rifle");
			break;
		case EWeaponType::EWT_Pistol:
			SectionName = FName("Rifle");
			break;
		case EWeaponType::EWT_RocketLauncher:
			SectionName = FName("Rifle");
		default:
			break;
		}
		AnimInstance->Montage_JumpToSection(SectionName);
	}
}
