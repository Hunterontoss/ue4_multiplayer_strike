#include "HUD/TW_OverheadWidget.h"

#include "Components/TextBlock.h"

void UTW_OverheadWidget::SetDisplayText(FString TextToDisplay)
{
	if (DisplayText)
	{
		DisplayText->SetText(FText::FromString(TextToDisplay));
	}
}

void UTW_OverheadWidget::ShowPlayerNetRole(APawn* InPawn)
{
	const ENetRole RemoteRole = InPawn->GetRemoteRole();
	const ENetRole LocalRole = InPawn->GetLocalRole();
	const FString L_RemoteRole = GetRoleName(RemoteRole);
	const FString L_LocalRole = GetRoleName(LocalRole);

	const FString RemoteRoleString = FString::Printf(TEXT("Remote Role: %s,  Local Role: %s"), *L_RemoteRole, *L_LocalRole);
	SetDisplayText(RemoteRoleString);
}

void UTW_OverheadWidget::NativeDestruct()
{
	RemoveFromParent();
	Super::NativeDestruct();
}

FString UTW_OverheadWidget::GetRoleName(const ENetRole Role)
{
	FString L_RemoteRole;
	switch (Role) {
	case ROLE_None:
		L_RemoteRole = FString("None");
		break;
	case ROLE_SimulatedProxy:
		L_RemoteRole = FString("Simulated Proxy");
		break;
	case ROLE_AutonomousProxy:
		L_RemoteRole = FString("Autonomous Proxy");
		break;
	case ROLE_Authority:
		L_RemoteRole = FString("Authority");
		break;
	}
	return L_RemoteRole;
}
