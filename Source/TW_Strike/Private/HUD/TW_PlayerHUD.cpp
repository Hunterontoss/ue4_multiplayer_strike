#include "HUD/TW_PlayerHUD.h"
#include "Blueprint/UserWidget.h"
#include "HUD/TW_MatchWaitingOverlay.h"
#include "HUD/TW_PlayerOverlay.h"


void ATW_PlayerHUD::BeginPlay()
{
	Super::BeginPlay();
}

void ATW_PlayerHUD::AddCharacterOverlay()
{
	UE_LOG(LogTemp, Warning, TEXT("1"))
	APlayerController* PlayerController = GetOwningPlayerController();
	if (PlayerController && CharacterOverlayClass)
	{
		CharacterOverlay = CreateWidget<UTW_PlayerOverlay>(PlayerController, CharacterOverlayClass);
		CharacterOverlay->AddToViewport();
	}
}

void ATW_PlayerHUD::AddMatchWaitingOverlay()
{
	APlayerController* PlayerController = GetOwningPlayerController();
	if (PlayerController && MatchWaitingClass)
	{
		MatchWaitingOverlay = CreateWidget<UTW_MatchWaitingOverlay>(PlayerController, MatchWaitingClass);
		MatchWaitingOverlay->AddToViewport();
	}
}

void ATW_PlayerHUD::DrawHUD()
{
	Super::DrawHUD();

	if (GEngine)
	{
		FVector2D ViewportSize;
		GEngine->GameViewport->GetViewportSize(ViewportSize);
		const FVector2D ViewportCenter(ViewportSize.X / 2.f, ViewportSize.Y / 2.f);

		const float SpreadScaled = CrosshairSpreadMax * HUDCrosshairs.CrosshairSpread;
		
		if (HUDCrosshairs.CrosshairsCenter)
		{
			const FVector2D T_Spread(0.f, 0.f);
			DrawCrosshair(HUDCrosshairs.CrosshairsCenter, ViewportCenter, T_Spread, HUDCrosshairs.CrosshairsColor);
		}
		if (HUDCrosshairs.CrosshairsLeft)
		{
			const FVector2D T_Spread(-SpreadScaled, 0.f);
			DrawCrosshair(HUDCrosshairs.CrosshairsLeft, ViewportCenter, T_Spread, HUDCrosshairs.CrosshairsColor);
		}
		if (HUDCrosshairs.CrosshairsRight)
		{
			const FVector2D T_Spread(SpreadScaled, 0.f);
			DrawCrosshair(HUDCrosshairs.CrosshairsRight, ViewportCenter, T_Spread, HUDCrosshairs.CrosshairsColor);
		}
		if (HUDCrosshairs.CrosshairsTop)
		{
			const FVector2D T_Spread(0.f, -SpreadScaled);
			DrawCrosshair(HUDCrosshairs.CrosshairsTop, ViewportCenter, T_Spread, HUDCrosshairs.CrosshairsColor);
		}
		if (HUDCrosshairs.CrosshairsBottom)
		{
			const FVector2D T_Spread(0.f, SpreadScaled);
			DrawCrosshair(HUDCrosshairs.CrosshairsBottom, ViewportCenter, T_Spread, HUDCrosshairs.CrosshairsColor);
		}
	}
}

void ATW_PlayerHUD::DrawCrosshair(UTexture2D* Texture, FVector2D ViewportCenter, FVector2D Spread, FLinearColor CrosshairColor)
{
	const float TextureWidth = Texture->GetSizeX();
	const float TextureHeight = Texture->GetSizeY();
	
	const FVector2D TextureDrawPoint(
		ViewportCenter.X - (TextureWidth / 2.f) + Spread.X,
		ViewportCenter.Y - (TextureHeight / 2.f) + Spread.Y
	);

	DrawTexture(
		Texture,
		TextureDrawPoint.X,
		TextureDrawPoint.Y,
		TextureWidth,
		TextureHeight,
		0.f,
		0.f,
		1.f,
		1.f,
		CrosshairColor
	);
}
