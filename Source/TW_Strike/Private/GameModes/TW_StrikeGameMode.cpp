#include "GameModes/TW_StrikeGameMode.h"
#include "GameFramework/PlayerStart.h"
#include "GameState/TW_GameState.h"
#include "Kismet/GameplayStatics.h"
#include "Players/TW_BaseCharacter.h"
#include "PlayerController/TW_PlayerController.h"
#include "PlayerState/TW_PlayerState.h"

namespace MatchState
{
	const FName Cooldown = FName("Cooldown");
}

ATW_StrikeGameMode::ATW_StrikeGameMode()
{
	bDelayedStart = true;
}

void ATW_StrikeGameMode::BeginPlay()
{
	Super::BeginPlay();

	LevelStartingTime = GetWorld()->GetTimeSeconds();
}

void ATW_StrikeGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (MatchState == MatchState::WaitingToStart)
	{
		CountdownTime = WarmupTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountdownTime <= 0.f)
		{
			StartMatch();
		}
	}
	else if (MatchState == MatchState::InProgress)
	{
		CountdownTime = WarmupTime + MatchTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountdownTime <= 0.f)
		{
			SetMatchState(MatchState::Cooldown);
		}
	}
	else if (MatchState == MatchState::Cooldown)
	{
		CountdownTime = CooldownTime + WarmupTime + MatchTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountdownTime <= 0.f)
		{
			UE_LOG(LogTemp, Display, TEXT("GAME MOD: Game Restarting"))
			RestartGame();
		}
	}
}

void ATW_StrikeGameMode::PlayerEliminated(ATW_BaseCharacter* ElimmedCharacter, ATW_PlayerController* VictimController,
                                          ATW_PlayerController* AttackerController)
{
	UpdatePlayerState(VictimController, AttackerController);
	
	if (ElimmedCharacter)
	{
		ElimmedCharacter->Eliminate();
	}
	
}

void ATW_StrikeGameMode::RequestRespawn(ACharacter* ElimmedCharacter, AController* ElimmedController)
{
	if (ElimmedCharacter)
	{
		ElimmedCharacter->Reset();
		ElimmedCharacter->Destroy();
	}
	if (ElimmedController)
	{
		TArray<AActor*> PlayerStarts;
		UGameplayStatics::GetAllActorsOfClass(this, APlayerStart::StaticClass(), PlayerStarts);
		const int32 Selection = FMath::RandRange(0, PlayerStarts.Num() - 1);
		RestartPlayerAtPlayerStart(ElimmedController, PlayerStarts[Selection]);
	}
}

void ATW_StrikeGameMode::UpdatePlayerState(ATW_PlayerController* VictimController, ATW_PlayerController* AttackerController) const
{
	if (AttackerController == nullptr || AttackerController->PlayerState == nullptr) return;
	if (VictimController == nullptr || VictimController->PlayerState == nullptr) return;
	
	ATW_PlayerState* AttackerPlayerState = AttackerController ? Cast<ATW_PlayerState>(AttackerController->PlayerState) : nullptr;
	ATW_PlayerState* VictimPlayerState = VictimController ? Cast<ATW_PlayerState>(VictimController->PlayerState) : nullptr;

	auto* L_GameState = GetGameState<ATW_GameState>();
	
	if (AttackerPlayerState && AttackerPlayerState != VictimPlayerState && L_GameState)
	{
		AttackerPlayerState->AddToScore(1.f);
		AttackerController->SetHUDScore(AttackerPlayerState->GetScore());
		L_GameState->UpdateTopScore(AttackerPlayerState);
	}
	
	if (VictimPlayerState)
	{
		VictimPlayerState->AddToDefeats(1);
		VictimController->SetHUDDefeats(VictimPlayerState->GetDefeats());
	}	
}

void ATW_StrikeGameMode::OnMatchStateSet()
{
	Super::OnMatchStateSet();

	for (auto It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		auto* L_PlayerController = Cast<ATW_PlayerController>(*It);
		if (L_PlayerController)
		{
			L_PlayerController->OnMatchStateSet(MatchState);
		}
	}
}
