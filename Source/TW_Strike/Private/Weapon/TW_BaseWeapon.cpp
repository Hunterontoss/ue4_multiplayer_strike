#include "Weapon/TW_BaseWeapon.h"
#include "Components/SphereComponent.h"
#include "Components/TW_PickupComponent.h"
#include "Components/WidgetComponent.h"
#include "Net/UnrealNetwork.h"
#include "Players/TW_BaseCharacter.h"
#include "Weapon/Projectiles/TW_Casing.h"
#include "Engine/SkeletalMeshSocket.h"
#include "PlayerController/TW_PlayerController.h"
#include "Weapon/Components/TW_AmmoComponent.h"
#include "Weapon/Components/TW_CrosshairComponent.h"

ATW_BaseWeapon::ATW_BaseWeapon()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
	
	////////////////////////////////////////////////////////////////////////////////////////
	///  Weapon Mesh
	///
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	SetRootComponent(WeaponMesh);
	
	WeaponMesh->SetCollisionResponseToChannels(ECollisionResponse::ECR_Block);
	WeaponMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	////////////////////////////////////////////////////////////////////////////////////////
	///  Pickup 
	///

	PickupComponent = CreateDefaultSubobject<UTW_PickupComponent>(TEXT("PickupComp"));

	PickupWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("PickupWidgetComp"));
	PickupWidget->SetupAttachment(RootComponent);
	
	////////////////////////////////////////////////////////////////////////////////////////

	CrosshairComponent = CreateDefaultSubobject<UTW_CrosshairComponent>(TEXT("CrosshairComp"));

	AmmoComponent = CreateDefaultSubobject<UTW_AmmoComponent>(TEXT("AmmoComp"));
	AmmoComponent->SetIsReplicated(true);
}


void ATW_BaseWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATW_BaseWeapon, WeaponState)
}

void ATW_BaseWeapon::BeginPlay()
{
	Super::BeginPlay();

	if (PickupWidget)
	{
		PickupWidget->SetVisibility(false);
	}
}

void ATW_BaseWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATW_BaseWeapon::SetWeaponState(const EWeaponState State)
{
	WeaponState = State;
	switch (WeaponState)
	{
	case EWeaponState::EWS_Equipped:
		WeaponMesh->SetSimulatePhysics(false);
		WeaponMesh->SetEnableGravity(false);
		WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		
		PickupComponent->AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		PickupWidget->SetVisibility(false);
		break;
	case EWeaponState::EWS_Initial:
		break;
	case EWeaponState::EWS_Dropped:
		PickupComponent->AreaSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		WeaponMesh->SetSimulatePhysics(true);
		WeaponMesh->SetEnableGravity(true);
		WeaponMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		
		break;
	default: ;
	}
}

void ATW_BaseWeapon::OnRep_WeaponState()
{
	switch (WeaponState)
	{
	case EWeaponState::EWS_Equipped:
		WeaponMesh->SetSimulatePhysics(false);
		WeaponMesh->SetEnableGravity(false);
		WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		
		PickupWidget->SetVisibility(false);
		PickupComponent->AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		break;
	case EWeaponState::EWS_Initial:
		break;
	case EWeaponState::EWS_Dropped:
		PickupComponent->AreaSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		WeaponMesh->SetSimulatePhysics(true);
		WeaponMesh->SetEnableGravity(true);
		WeaponMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		break;
	default:
		break;
	}
}

void ATW_BaseWeapon::DroppedWeapon()
{
	SetWeaponState(EWeaponState::EWS_Dropped);
	
	const FDetachmentTransformRules DetachRules(EDetachmentRule::KeepWorld, true);
	WeaponMesh->DetachFromComponent(DetachRules);
	SetOwner(nullptr);
}

void ATW_BaseWeapon::SetPickupWidgetVisibility_Implementation(bool bVisible)
{
	PickupWidget->SetVisibility(bVisible);	
}

void ATW_BaseWeapon::Fire(const FVector& HitTarget)
{
	AmmoComponent->SpendRound();
}

void ATW_BaseWeapon::FireEffects(const FVector_NetQuantize& TraceHitTarget)
{
	if (FireAnimation)
	{
		WeaponMesh->PlayAnimation(FireAnimation, false);
	}
	
	if (CasingClass)
	{
		const USkeletalMeshSocket* AmmoEjectSocket = WeaponMesh->GetSocketByName(FName("AmmoEject"));
		if (AmmoEjectSocket)
		{
			const FTransform SocketTransform = AmmoEjectSocket->GetSocketTransform(WeaponMesh);

			UWorld* World = GetWorld();
			if (World)
			{
				World->SpawnActor<ATW_Casing>(
					CasingClass,
					SocketTransform.GetLocation(),
					SocketTransform.GetRotation().Rotator()
				);
			}
		}
	}

	SetHUDAmmo(AmmoComponent->GetAmmo());
}

void ATW_BaseWeapon::SetHUDAmmo(int32 Ammo)
{
	if (GetPlayerControllerIsValid())
	{
		GetPlayerControllerIsValid()->SetHUDWeaponAmmo(Ammo);
	}
}

void ATW_BaseWeapon::OnRep_Owner()
{
	Super::OnRep_Owner();
	
	SetHUDAmmo(AmmoComponent->GetAmmo());
}

ATW_PlayerController* ATW_BaseWeapon::GetPlayerControllerIsValid() const
{
	const auto* L_Character = Cast<ATW_BaseCharacter>(GetOwner());

	if (L_Character && L_Character->Controller)
	{
		auto* L_Controller = Cast<ATW_PlayerController>(L_Character->Controller);

		if (L_Controller)
		{
			return L_Controller;
		}
	}
	return nullptr;
}