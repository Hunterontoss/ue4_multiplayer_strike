#include "Weapon/TW_HitScanWeapon.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Players/TW_BaseCharacter.h"


DEFINE_LOG_CATEGORY_STATIC(LogHitScanWeapon, All, All);

void ATW_HitScanWeapon::Fire(const FVector& HitTarget)
{
	Super::Fire(HitTarget);

	AController* InstigatorController = GetOwnerInstigatorController();
	USkeletalMeshSocket const* MuzzleFlashSocket = GetWeaponMesh()->GetSocketByName("MuzzleFlash");
	
	if (MuzzleFlashSocket)
	{
		FTransform SocketTransform = MuzzleFlashSocket->GetSocketTransform(GetWeaponMesh());
		FVector Start = SocketTransform.GetLocation();
		
		FVector End =  Start + (HitTarget - Start) * 1.25f;

		UWorld* World = GetWorld();
		
		if (World)
		{
			FHitResult FireHit;
			World->LineTraceSingleByChannel(
				FireHit,
				Start,
				End,
				ECollisionChannel::ECC_Visibility
			);

			
			MakeDamage(FireHit, InstigatorController);

			FVector BeamEnd = FireHit.bBlockingHit ? FireHit.ImpactPoint : End;
			
			if (BeamParticles)
			{
				UParticleSystemComponent* Beam = UGameplayStatics::SpawnEmitterAtLocation(
					World,
					BeamParticles,
					SocketTransform
				);
				if (Beam)
				{
					Beam->SetVectorParameter(FName("Target"), BeamEnd);
				}
			}
			
		}
	}
}

AController* ATW_HitScanWeapon::GetOwnerInstigatorController() const
{
	const auto* OwnerPawn = Cast<APawn>(GetOwner());
	if (OwnerPawn == nullptr)
	{
		UE_LOG(LogHitScanWeapon, Warning, TEXT("Owner is null"))
		return nullptr;	
	}
	
	return OwnerPawn->GetController();
}

void ATW_HitScanWeapon::MakeDamage(const FHitResult& FireHit, AController* InstigatorController)
{
	if (FireHit.bBlockingHit)
	{
		auto* L_TargetCharacter = Cast<ATW_BaseCharacter>(FireHit.GetActor());
		if (L_TargetCharacter && HasAuthority() && InstigatorController)
		{
			UGameplayStatics::ApplyDamage(
				L_TargetCharacter,
				Damage,
				InstigatorController,
				this,
				UDamageType::StaticClass()
			);
		}
		
		if (ImpactParticles)
		{
			UGameplayStatics::SpawnEmitterAtLocation(
				GetWorld(),
				ImpactParticles,
				FireHit.ImpactPoint,
				FireHit.ImpactNormal.Rotation()
			);
		}
	}
}
