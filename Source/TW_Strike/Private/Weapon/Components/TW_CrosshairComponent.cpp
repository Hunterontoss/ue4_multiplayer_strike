#include "Weapon/Components/TW_CrosshairComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Players/TW_BaseCharacter.h"

UTW_CrosshairComponent::UTW_CrosshairComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	
}

void UTW_CrosshairComponent::BeginPlay()
{
	Super::BeginPlay();
}

FHUDCrosshairs UTW_CrosshairComponent::CalculateCrosshairData(const float DeltaTime, ATW_BaseCharacter* PlayerRef)
{
	CalculateCrosshairSpread(DeltaTime, PlayerRef);
	return MakeCrosshairData();
}


FHUDCrosshairs UTW_CrosshairComponent::MakeCrosshairData() const
{
	FHUDCrosshairs HUDPackage;
	HUDPackage.CrosshairsCenter = CrosshairsCenter;
	HUDPackage.CrosshairsLeft = CrosshairsLeft;
	HUDPackage.CrosshairsRight = CrosshairsRight;
	HUDPackage.CrosshairsBottom = CrosshairsBottom;
	HUDPackage.CrosshairsTop = CrosshairsTop;

	const auto Factor = CrosshairVelocityFactor + CrosshairInAirFactor - CrosshairAimFactor + CrosshairShootingFactor;
	HUDPackage.CrosshairSpread = 0.5f + Factor;

	return HUDPackage;
}

void UTW_CrosshairComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UTW_CrosshairComponent::CalculateCrosshairSpread(float DeltaTime, ATW_BaseCharacter* CharRef)
{
	if (!CharRef) return;
	// [0, 600] -> [0, 1]
	const FVector2D WalkSpeedRange(0.f, CharRef->GetCharacterMovement()->MaxWalkSpeed);
	const FVector2D VelocityMultiplierRange(0.f, 1.f);
	FVector Velocity = CharRef->GetVelocity();
	Velocity.Z = 0.f;

	//
	CrosshairVelocityFactor = FMath::GetMappedRangeValueClamped(WalkSpeedRange, VelocityMultiplierRange, Velocity.Size());

	if (CharRef->GetCharacterMovement()->IsFalling())
	{
		CrosshairInAirFactor = FMath::FInterpTo(CrosshairInAirFactor, 2.25f, DeltaTime, 2.25f);
	}
	else
	{
		CrosshairInAirFactor = FMath::FInterpTo(CrosshairInAirFactor, 0.f, DeltaTime, 30.f);
	}

	if (CharRef->IsAiming())
	{
		CrosshairAimFactor = FMath::FInterpTo(CrosshairAimFactor, 0.58f, DeltaTime, 30.f);
	}
	else
	{
		CrosshairAimFactor = FMath::FInterpTo(CrosshairAimFactor, 0.f, DeltaTime, 30.f);
	}

	CrosshairShootingFactor = FMath::FInterpTo(CrosshairShootingFactor, 0.f, DeltaTime, 40.f);
}

