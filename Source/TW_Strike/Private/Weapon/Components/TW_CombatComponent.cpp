#include "Weapon/Components/TW_CombatComponent.h"
#include "Components/TW_TraceComponent.h"
#include "Components/TW_ZoomCameraComponent.h"
#include "Components/WidgetComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "HUD/TW_PlayerHUD.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "PlayerController/TW_PlayerController.h"
#include "Players/TW_BaseCharacter.h"
#include "Sound/SoundCue.h"
#include "Weapon/TW_BaseWeapon.h"
#include "Weapon/Components/TW_AmmoComponent.h"
#include "Weapon/Components/TW_CrosshairComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogCombatComponent, All, All);

UTW_CombatComponent::UTW_CombatComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	
}

void UTW_CombatComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTW_CombatComponent, EquippedWeapon);
	DOREPLIFETIME(UTW_CombatComponent, bAiming);
	DOREPLIFETIME_CONDITION(UTW_CombatComponent, CarriedAmmo, COND_OwnerOnly);
	DOREPLIFETIME(UTW_CombatComponent, CombatState);
}

void UTW_CombatComponent::HideWeaponMesh(const bool bNeedHide) const
{
	if (EquippedWeapon && EquippedWeapon->GetWeaponMesh())
	{
		EquippedWeapon->GetWeaponMesh()->bOwnerNoSee = bNeedHide;
	}
}

void UTW_CombatComponent::BeginPlay()
{
	Super::BeginPlay();

	if (Character)
	{
		Character->GetCharacterMovement()->MaxWalkSpeed = BaseWalkSpeed;
	}
	
	if (GetOwner() && GetOwner()->FindComponentByClass<UTW_TraceComponent>())
	{
		TraceComp = GetOwner()->FindComponentByClass<UTW_TraceComponent>();
		ZoomComp = GetOwner()->FindComponentByClass<UTW_ZoomCameraComponent>();
	}

	if (Character->HasAuthority())
	{
		InitializeCarriedAmmo();
	}
}

void UTW_CombatComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);


	if (Character && Character->IsLocallyControlled())
	{
		// Search Target using Crosshair  
		FHitResult HitResult;
		TraceComp->TraceUnderCrosshairs(HitResult);
		HitTarget = HitResult.ImpactPoint;
		
		// DrawCrosshair 
		SetHUDCrosshairs(DeltaTime, HitResult);

		// Zoom
		if (EquippedWeapon && ZoomComp)
		{
			ZoomComp->InterpFOV(DeltaTime, EquippedWeapon->ZoomedFOV, EquippedWeapon->ZoomInterpSpeed);
		}
	}
}


void UTW_CombatComponent::SetHUDCrosshairs(float DeltaTime, FHitResult HitResult)
{
	CheckHUDIsValid();

	if (HUD && EquippedWeapon && EquippedWeapon->CrosshairComponent)
	{
		FHUDCrosshairs Data =  EquippedWeapon->CrosshairComponent->CalculateCrosshairData(DeltaTime, Character);

		if (HitResult.GetActor() && HitResult.GetActor()->Implements<UTW_InteractWithCroshairInterface>())
		{
			Data.CrosshairsColor = FLinearColor::Red;
		}
		else
		{
			Data.CrosshairsColor = FLinearColor::White;
		}
		
		HUD->SetHUDPackage(Data);
	}
}

bool UTW_CombatComponent::CheckHUDIsValid()
{
	if (Character == nullptr || Character->Controller == nullptr) return false; 
	
	Controller = Controller == nullptr ? Cast<ATW_PlayerController>(Character->Controller) : Controller;
	
	if (Controller)
	{
		HUD = HUD == nullptr ? Cast<ATW_PlayerHUD>(Controller->GetHUD()) : HUD;

		if (HUD)
		{
			return true;
		}
	}
	
	return false;
}

void UTW_CombatComponent::EquipWeapon(ATW_BaseWeapon* WeaponToEquip)
{
	if (!Character || !WeaponToEquip) return;

	if (EquippedWeapon)
	{
		EquippedWeapon->DroppedWeapon();
	}
	
	EquippedWeapon = WeaponToEquip;
	EquippedWeapon->SetWeaponState(EWeaponState::EWS_Equipped);
	
	const USkeletalMeshSocket* HandSocket = Character->GetMesh()->GetSocketByName(FName("RightHandSocket"));
	if (HandSocket)
	{
		HandSocket->AttachActor(EquippedWeapon, Character->GetMesh());
	}
	
	EquippedWeapon->SetOwner(Character);
	EquippedWeapon->SetHUDAmmo(EquippedWeapon->AmmoComponent->GetAmmo());

	
	if (CarriedAmmoMap.Contains(EquippedWeapon->GetWeaponType()))
	{
		CarriedAmmo = CarriedAmmoMap[EquippedWeapon->GetWeaponType()];
	}
	Controller = Controller == nullptr ? Cast<ATW_PlayerController>(Character->Controller) : Controller;
	Controller->SetHUDCarriedAmmo(CarriedAmmo);
	
	Character->GetCharacterMovement()->bOrientRotationToMovement = false;
	Character->bUseControllerRotationYaw = true;

	PlayEquipSound();
	if (EquippedWeapon->AmmoComponent->IsEmpty())
	{
		ReloadCurrentWeapon();
	}
}

void UTW_CombatComponent::OnRep_EquippedWeapon()
{
	if (EquippedWeapon && Character)
	{
		EquippedWeapon->SetWeaponState(EWeaponState::EWS_Equipped);
		const USkeletalMeshSocket* HandSocket = Character->GetMesh()->GetSocketByName(FName("RightHandSocket"));
		if (HandSocket)
		{
			HandSocket->AttachActor(EquippedWeapon, Character->GetMesh());
		}
		
		Character->GetCharacterMovement()->bOrientRotationToMovement = false;
		Character->bUseControllerRotationYaw = true;

		PlayEquipSound();
	}
	
}

void UTW_CombatComponent::PlayEquipSound() const
{
	if (EquippedWeapon->EquipSound)
	{
		UGameplayStatics::PlaySoundAtLocation(
			this,
			EquippedWeapon->EquipSound,
			Character->GetActorLocation()
		);
	}
}

void UTW_CombatComponent::DropWeapon()
{
	if (EquippedWeapon)
	{
		EquippedWeapon->DroppedWeapon();
	}
}

void UTW_CombatComponent::SetAiming(bool bIsAiming)
{
	bAiming = bIsAiming;
	ServerSetAiming(bIsAiming);
	
	ChangeSpeedWhenAim();
}

void UTW_CombatComponent::ServerSetAiming_Implementation(bool bIsAiming)
{
	bAiming = bIsAiming;
	
	ChangeSpeedWhenAim();
}

void UTW_CombatComponent::ChangeSpeedWhenAim()
{
	if (Character)
	{
		Character->GetCharacterMovement()->MaxWalkSpeed = bAiming ? AimWalkSpeed : BaseWalkSpeed;
	}
}

////////////////////////////////////////////////////////////////////////////////////////
/// FIRE  
///
bool UTW_CombatComponent::CanFire() const
{
	if (EquippedWeapon == nullptr) return false;
	
	return !EquippedWeapon->AmmoComponent->IsEmpty() && bCanFire && CombatState == ECombatState::ECS_Unoccupied;
}
void UTW_CombatComponent::StartFireTimer()
{
	if (EquippedWeapon == nullptr || Character == nullptr) return;
	Character->GetWorldTimerManager().SetTimer(
		FireTimer_Handle,
		this,
		&UTW_CombatComponent::FinishedFireTimer,
		EquippedWeapon->FireDelay
	);
}

void UTW_CombatComponent::FinishedFireTimer()
{
	if (EquippedWeapon == nullptr) return;
	
	bCanFire = true;
	if (bFireButtonPressed && EquippedWeapon->bAutomatic)
	{
		Fire();
	}
	
	if (EquippedWeapon->AmmoComponent->IsEmpty())
	{
		ReloadCurrentWeapon();
	}
}

void UTW_CombatComponent::FireButtonPressed(bool bIspPressed)
{
	bFireButtonPressed = bIspPressed;

	if (bFireButtonPressed)
	{
		if (CanFire())
		{
			Fire();
		}
	}
}

void UTW_CombatComponent::Fire()
{
	FHitResult HitResult;
	TraceComp->TraceUnderCrosshairs(HitResult);
	ServerFire(HitResult.ImpactPoint);
	StartFireTimer();
	bCanFire = false;
}

void UTW_CombatComponent::ServerFire_Implementation(const FVector_NetQuantize& TraceHitTarget)
{
	if (CanFire())
	{
		EquippedWeapon->Fire(TraceHitTarget);
		MulticastFire(TraceHitTarget);
		ClientFire();	
	}
}

void UTW_CombatComponent::MulticastFire_Implementation(const FVector_NetQuantize& TraceHitTarget)
{
	if (EquippedWeapon == nullptr) return;
	
	if (Character && CombatState == ECombatState::ECS_Unoccupied)
	{
		Character->PlayFireMontage(bAiming);
		EquippedWeapon->FireEffects(TraceHitTarget);
	}
}

void UTW_CombatComponent::ClientFire_Implementation()
{
}

///   
/// End Fire
////////////////////////////////////////////////////////////////////////////////////////

void UTW_CombatComponent::InitializeCarriedAmmo()
{
	CarriedAmmoMap.Emplace(EWeaponType::EWT_AssaultRifle, StartingARAmmo);
	CarriedAmmoMap.Emplace(EWeaponType::EWT_Pistol, StartingPistolAmmo);
	CarriedAmmoMap.Emplace(EWeaponType::EWT_RocketLauncher, StartingRocketLauncherAmmo);
	CarriedAmmoMap.Emplace(EWeaponType::EWT_SniperRifle, StartingSniperRifleAmmo);
}

void UTW_CombatComponent::OnRep_CarriedAmmo()
{
	if (Controller)
	{
		Controller->SetHUDCarriedAmmo(CarriedAmmo);
	}
}


void UTW_CombatComponent::ReloadCurrentWeapon()
{
	if (EquippedWeapon == nullptr) return;

	if (EquippedWeapon->AmmoComponent->GetAmmo() == EquippedWeapon->AmmoComponent->GetMagCapacity()) return;
		
	if (CarriedAmmo > 0 && CombatState != ECombatState::ECS_Reloading)
	{
		ServerReload();
	}
}

void UTW_CombatComponent::ServerReload_Implementation()
{
	if (Character == nullptr || EquippedWeapon == nullptr) return;

	CombatState = ECombatState::ECS_Reloading;
	HandleReload();
}

void UTW_CombatComponent::OnRep_CombatState()
{
	switch (CombatState)
	{
	case ECombatState::ECS_Reloading:
		HandleReload();
		break;
	case ECombatState::ECS_Unoccupied:
		if (bFireButtonPressed)
		{
			Fire();
		}
		break;
	default:
		break;
	}
}

void UTW_CombatComponent::HandleReload() const
{
	if (Character)
	{
		Character->PlayReloadMontage(EquippedWeapon->GetWeaponType());	
	}
}

void UTW_CombatComponent::UpdateAmmoValues()
{
	if (Character == nullptr || EquippedWeapon == nullptr) return;

	const int32 ReloadAmount = AmountToReload();
	
	if (CarriedAmmoMap.Contains(EquippedWeapon->GetWeaponType()))
	{
		CarriedAmmoMap[EquippedWeapon->GetWeaponType()] -= ReloadAmount;
		CarriedAmmo = CarriedAmmoMap[EquippedWeapon->GetWeaponType()];
	}

	EquippedWeapon->AmmoComponent->TryAddAmmo(ReloadAmount);
	
	Controller = Controller == nullptr ? Cast<ATW_PlayerController>(Character->Controller) : Controller;
	if (Controller)
	{
		Controller->SetHUDCarriedAmmo(CarriedAmmo);
	}
}

int32 UTW_CombatComponent::AmountToReload()
{
	if (EquippedWeapon == nullptr) return 0;
	
	const int32 RoomInMag = EquippedWeapon->AmmoComponent->GetMagCapacity() - EquippedWeapon->AmmoComponent->GetAmmo();

	if (CarriedAmmoMap.Contains(EquippedWeapon->GetWeaponType()))
	{
		const int32 AmountCarried = CarriedAmmoMap[EquippedWeapon->GetWeaponType()];
		const int32 Least = FMath::Min(RoomInMag, AmountCarried);

		// defense if Ammo > MagCapacity
		return FMath::Clamp(RoomInMag, 0, Least);
	}
	return 0;
}

void UTW_CombatComponent::FinishReloading()
{
	if (Character == nullptr) return;
	
	if (Character->HasAuthority())
	{
		CombatState = ECombatState::ECS_Unoccupied;
		UpdateAmmoValues();
	}

	if (bFireButtonPressed)
	{
		Fire();
	}
}

ECombatState UTW_CombatComponent::GetCombatState() const
{
	return CombatState;
}
