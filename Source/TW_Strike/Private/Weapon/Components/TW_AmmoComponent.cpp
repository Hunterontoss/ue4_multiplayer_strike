#include "Weapon/Components/TW_AmmoComponent.h"
#include "Net/UnrealNetwork.h"
#include "Weapon/TW_BaseWeapon.h"

UTW_AmmoComponent::UTW_AmmoComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

}

void UTW_AmmoComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTW_AmmoComponent, Ammo)
}

void UTW_AmmoComponent::BeginPlay()
{
	Super::BeginPlay();

	ComponentOwner = GetOwner<ATW_BaseWeapon>();

	check(ComponentOwner)
}

void UTW_AmmoComponent::SpendRound()
{
	Ammo = FMath::Clamp(Ammo - 1, 0, MagCapacity);
}

void UTW_AmmoComponent::TryAddAmmo(int32 AmmoToAdd)
{
	Ammo = FMath::Clamp(Ammo + AmmoToAdd, 0, MagCapacity);
	
	ComponentOwner->SetHUDAmmo(Ammo);
}

void UTW_AmmoComponent::OnRep_Ammo()
{
	ComponentOwner->SetHUDAmmo(Ammo);
}

bool UTW_AmmoComponent::IsEmpty() const
{
	return Ammo <= 0;	
}
