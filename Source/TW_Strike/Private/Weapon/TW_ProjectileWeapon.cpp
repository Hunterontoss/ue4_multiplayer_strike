#include "Weapon/TW_ProjectileWeapon.h"
#include "Weapon/Projectiles/TW_BaseProjectile.h"
#include "Engine/SkeletalMeshSocket.h"

DEFINE_LOG_CATEGORY_STATIC(LogProjectileWeapon, All, All);

void ATW_ProjectileWeapon::Fire(const FVector& HitTarget)
{
	Super::Fire(HitTarget);

	if (!HasAuthority()) return;
	
	APawn* L_InstigatorPawn = Cast<APawn>(GetOwner());
	if (!L_InstigatorPawn)
	{
		UE_LOG(LogProjectileWeapon, Warning, TEXT("InstigatorPawn Is Null"))
		return;
	}

	if (!ProjectileClass)
	{
		UE_LOG(LogProjectileWeapon, Warning, TEXT("ProjectileClass Is Null"))
		return;
	}

	const USkeletalMeshSocket* L_MuzzleFlashSocket = GetWeaponMesh()->GetSocketByName(FName("MuzzleFlash"));

	if (L_MuzzleFlashSocket)
	{
		const FTransform L_SocketTransform = L_MuzzleFlashSocket->GetSocketTransform(GetWeaponMesh());
		const FVector ToTarget = HitTarget - L_SocketTransform.GetLocation();

		const FRotator L_TargetRotation  = ToTarget.Rotation();

		FActorSpawnParameters L_SpawnParams;
		L_SpawnParams.Owner = GetOwner();
		L_SpawnParams.Instigator = L_InstigatorPawn;

		if (GetWorld())
		{
			GetWorld()->SpawnActor<ATW_BaseProjectile>(ProjectileClass, L_SocketTransform.GetLocation(), L_TargetRotation, L_SpawnParams);
		}
	}
	else
	{
		UE_LOG(LogProjectileWeapon, Warning, TEXT("MuzzleFlashSocket is Null"))
	}
}
