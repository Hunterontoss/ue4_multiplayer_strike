#include "Weapon/Projectiles/TW_RocketProjectile.h"

#include "Kismet/GameplayStatics.h"

ATW_RocketProjectile::ATW_RocketProjectile()
{
	RocketMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Rocket Mesh"));
	RocketMesh->SetupAttachment(RootComponent);
	RocketMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ATW_RocketProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	const APawn* FiringPawn = GetInstigator();
	if (FiringPawn)
	{
		auto* L_FiringController = FiringPawn->GetController();
		if (L_FiringController)
		{
			UGameplayStatics::ApplyRadialDamageWithFalloff(
				this,                                      // World context object
				Damage,                                    // BaseDamage
				10.f,                                      // MinimumDamage
				GetActorLocation(),                        // Origin 
				DamageInnerRadius,                          
				DamageOuterRadius,                                   
				1.f,                                       // DamageFalloff
				UDamageType::StaticClass(),				   // DamageTypeClass
				TArray<AActor*>(),						   // IgnoreActors
				this,                                      // DamageCauser
				L_FiringController                        // InstigatorController
			);
		}
	}

	
	Super::OnHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}
