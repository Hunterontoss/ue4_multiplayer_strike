#include "PlayerController/TW_PlayerController.h"

#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "Components/TW_HealthComponent.h"
#include "GameFramework/GameMode.h"
#include "GameModes/TW_StrikeGameMode.h"
#include "GameState/TW_GameState.h"
#include "HUD/TW_MatchWaitingOverlay.h"
#include "HUD/TW_PlayerHUD.h"
#include "HUD/TW_PlayerOverlay.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Players/TW_BaseCharacter.h"
#include "PlayerState/TW_PlayerState.h"
#include "Weapon/Components/TW_CombatComponent.h"

void ATW_PlayerController::BeginPlay()
{
	Super::BeginPlay();

	PlayerHUD = Cast<ATW_PlayerHUD>(GetHUD());
	
	ServerCheckMatchState();
}

void ATW_PlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	SetHUDTime();
	CheckTimeSync(DeltaSeconds);
	PollInit();
}

void ATW_PlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATW_PlayerController, MatchState)
}

void ATW_PlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	ATW_BaseCharacter* PlayerCharacter = Cast<ATW_BaseCharacter>(InPawn);
	if (PlayerCharacter)
	{
		SetHUDHealth(PlayerCharacter->HealthComponent->GetHealth(), PlayerCharacter->HealthComponent->GetHealth());
	}
}


void ATW_PlayerController::OnMatchStateSet(FName State)
{
	MatchState = State;

	if (MatchState == MatchState::InProgress)
	{
		HandleMatchHasStarted();
	}
	else if (MatchState == MatchState::Cooldown)
	{
		HandleCooldown();
	}
}

void ATW_PlayerController::OnRep_MatchState()
{
	if (MatchState == MatchState::InProgress)
	{
		HandleMatchHasStarted();
	}
	else if (MatchState == MatchState::Cooldown)
	{
		HandleCooldown();
	}
}

void ATW_PlayerController::HandleMatchHasStarted()
{
	if (IsHUD_Valid())
	{
		PlayerHUD->AddCharacterOverlay();
		if (PlayerHUD->MatchWaitingOverlay)
		{
			PlayerHUD->MatchWaitingOverlay->SetVisibility(ESlateVisibility::Hidden);
		}
	}
}

void ATW_PlayerController::HandleCooldown()
{
	if (IsHUD_Valid())
	{
		PlayerHUD->CharacterOverlay->RemoveFromParent();
		if (PlayerHUD->MatchWaitingOverlay && PlayerHUD->MatchWaitingOverlay->AnnouncementText && PlayerHUD->MatchWaitingOverlay->InfoText)
		{
			PlayerHUD->MatchWaitingOverlay->SetVisibility(ESlateVisibility::Visible);
			const FString AnnouncementText("New Match Starts In:");
			PlayerHUD->MatchWaitingOverlay->AnnouncementText->SetText(FText::FromString(AnnouncementText));

			// Info Text

			const auto* L_GameState = Cast<ATW_GameState>(UGameplayStatics::GetGameState(this));
			const auto* L_PlayerState = GetPlayerState<ATW_PlayerState>();
			
			if (L_GameState && L_PlayerState)
			{
				TArray<ATW_PlayerState*> TopPlayers = L_GameState->TopScoringPlayers;
				
				FString InfoTextString;
				if (TopPlayers.Num() == 0)
				{
					InfoTextString = FString("There is no winner.");
				}
				else if (TopPlayers.Num() == 1 && TopPlayers[0] == L_PlayerState)
				{
					InfoTextString = FString("You are the winner!");
				}
				else if (TopPlayers.Num() == 1)
				{
					InfoTextString = FString::Printf(TEXT("Winner: \n%s"), *TopPlayers[0]->GetPlayerName());
				}
				else if (TopPlayers.Num() > 1)
				{
					InfoTextString = FString("Players tied for the win:\n");
					
					for (const auto TiedPlayer : TopPlayers)
					{
						InfoTextString.Append(FString::Printf(TEXT("%s\n"), *TiedPlayer->GetPlayerName()));
					}
				}

				PlayerHUD->MatchWaitingOverlay->InfoText->SetText(FText::FromString(InfoTextString));
			}
		}
	}

	auto* L_Character = Cast<ATW_BaseCharacter>(GetPawn());
	if (L_Character && L_Character->CombatComponent)
	{
		L_Character->bDisableGameplay = true;
		L_Character->CombatComponent->FireButtonPressed(false);
	}
}

void ATW_PlayerController::PollInit()
{
	if (CharacterOverlay == nullptr)
	{
		if (PlayerHUD && PlayerHUD->CharacterOverlay)
		{
			CharacterOverlay = PlayerHUD->CharacterOverlay;
			if (CharacterOverlay)
			{
				SetHUDHealth(HUDHealth, HUDMaxHealth);
				SetHUDScore(HUDScore);
				SetHUDDefeats(HUDDefeats);
			}
		}
	}
}

bool ATW_PlayerController::IsHUD_Valid()
{
	PlayerHUD = PlayerHUD == nullptr ? Cast<ATW_PlayerHUD>(GetHUD()) : PlayerHUD;

	const bool bHUDValid = IsValid(PlayerHUD);
	
	return bHUDValid;
}

void ATW_PlayerController::SetHUDHealth(float Health, float MaxHealth)
{
	const bool bHUDValid = IsHUD_Valid() &&
			PlayerHUD->CharacterOverlay &&
			PlayerHUD->CharacterOverlay->HealthBar &&
			PlayerHUD->CharacterOverlay->HealthText;
	
	if (bHUDValid)
	{
		const float HealthPercent = Health / MaxHealth;
		PlayerHUD->CharacterOverlay->HealthBar->SetPercent(HealthPercent);

		const FString HealthText = FString::Printf(TEXT("%d/%d"), FMath::CeilToInt(Health), FMath::CeilToInt(MaxHealth));
		
		PlayerHUD->CharacterOverlay->HealthText->SetText(FText::FromString(HealthText));
	}
	else
	{
		bInitializeCharacterOverlay = true;
		HUDHealth = Health;
		HUDMaxHealth = MaxHealth;
	}
}

void ATW_PlayerController::SetHUDScore(float Score)
{
	const bool bHUDValid = IsHUD_Valid() &&
		PlayerHUD->CharacterOverlay &&
		PlayerHUD->CharacterOverlay->ScoreAmount;
	
	if (bHUDValid)
	{
		const FString ScoreText = FString::Printf(TEXT("%d"), FMath::FloorToInt(Score));
		PlayerHUD->CharacterOverlay->ScoreAmount->SetText(FText::FromString(ScoreText));
	}
	else
	{
		bInitializeCharacterOverlay = true;
		HUDScore = Score;
	}
}

void ATW_PlayerController::SetHUDDefeats(int32 Defeats)
{
	const bool bHUDValid = IsHUD_Valid() &&
		PlayerHUD->CharacterOverlay &&
		PlayerHUD->CharacterOverlay->DefeatsAmount;
	
	if (bHUDValid)
	{
		const FString DefeatsText = FString::Printf(TEXT("%d"), Defeats);
		PlayerHUD->CharacterOverlay->DefeatsAmount->SetText(FText::FromString(DefeatsText));
	}
	else
	{
		bInitializeCharacterOverlay = true;
		HUDDefeats = Defeats;
	}
}

void ATW_PlayerController::SetHUDWeaponAmmo(int32 Ammo)
{
	const bool bHUDValid = IsHUD_Valid() &&
		PlayerHUD->CharacterOverlay &&
		PlayerHUD->CharacterOverlay->WeaponAmmoAmount;
	
	if (bHUDValid)
	{
		const FString DefeatsText = FString::Printf(TEXT("%d"), Ammo);
		PlayerHUD->CharacterOverlay->WeaponAmmoAmount->SetText(FText::FromString(DefeatsText));
	}
}

void ATW_PlayerController::SetHUDCarriedAmmo(int32 Ammo)
{
	const bool bHUDValid = IsHUD_Valid() &&
		PlayerHUD->CharacterOverlay &&
		PlayerHUD->CharacterOverlay->CarriedAmmoAmount;
	
	if (bHUDValid)
	{
		const FString DefeatsText = FString::Printf(TEXT("%d"), Ammo);
		PlayerHUD->CharacterOverlay->CarriedAmmoAmount->SetText(FText::FromString(DefeatsText));
	}
}

void ATW_PlayerController::SetHUDMatchCountdown(float CountdownTime)
{
	const bool bHUDValid = IsHUD_Valid() &&
		PlayerHUD->CharacterOverlay &&
		PlayerHUD->CharacterOverlay->MatchTimeText;
	
	if (bHUDValid)
	{
		if (CountdownTime < 0.f)
		{
			PlayerHUD->CharacterOverlay->MatchTimeText->SetText(FText());
			return;
		}
		
		const int32 Minutes = FMath::FloorToInt(CountdownTime / 60.f);
		const int32 Seconds = CountdownTime - Minutes * 60;

		const FString CountdownText = FString::Printf(TEXT("%02d:%02d"), Minutes, Seconds);
		PlayerHUD->CharacterOverlay->MatchTimeText->SetText(FText::FromString(CountdownText));
	}
}

void ATW_PlayerController::SetHUDWaitingMatch(float CountdownTime)
{
	const bool bHUDValid = IsHUD_Valid() &&
		PlayerHUD->MatchWaitingOverlay &&
		PlayerHUD->MatchWaitingOverlay->WarmupTime;
	
	if (bHUDValid)
	{
		if (CountdownTime < 0.f)
		{
			PlayerHUD->MatchWaitingOverlay->WarmupTime->SetText(FText());
			return;
		}
		
		const int32 Minutes = FMath::FloorToInt(CountdownTime / 60.f);
		const int32 Seconds = CountdownTime - Minutes * 60;

		const FString CountdownText = FString::Printf(TEXT("%02d:%02d"), Minutes, Seconds);
		PlayerHUD->MatchWaitingOverlay->WarmupTime->SetText(FText::FromString(CountdownText));
	}
}

void ATW_PlayerController::SetHUDTime()
{
	float L_TimeLeft = 0.f;
	if (MatchState == MatchState::WaitingToStart)
	{
		L_TimeLeft = WarmupTime - GetServerTime() + LevelStartingTime;
	}
	else if (MatchState == MatchState::InProgress)
	{
		L_TimeLeft = WarmupTime + MatchTime - GetServerTime() + LevelStartingTime;
	}
	else if (MatchState == MatchState::Cooldown)
	{
		L_TimeLeft = CooldownTime + WarmupTime + MatchTime - GetServerTime() + LevelStartingTime;
	}

	const uint32 SecondsLeft = FMath::CeilToInt(L_TimeLeft);
	if (CountdownInt != SecondsLeft)
	{
		if (MatchState == MatchState::WaitingToStart || MatchState == MatchState::Cooldown)
			{
				SetHUDWaitingMatch(L_TimeLeft);
			}
		if (MatchState == MatchState::InProgress)
			{
				SetHUDMatchCountdown(L_TimeLeft);
			}
	}

	CountdownInt = SecondsLeft;
}

void ATW_PlayerController::ServerCheckMatchState_Implementation()
{
	const auto* GameMode = Cast<ATW_StrikeGameMode>(UGameplayStatics::GetGameMode(this));
	if (GameMode)
	{
		WarmupTime = GameMode->WarmupTime;
		MatchTime = GameMode->MatchTime;
		CooldownTime = GameMode->CooldownTime;
		LevelStartingTime = GameMode->LevelStartingTime;
		MatchState = GameMode->GetMatchState();
		ClientJoinMidgame(MatchState, WarmupTime, MatchTime, CooldownTime, LevelStartingTime);
	}
}

void ATW_PlayerController::ClientJoinMidgame_Implementation(FName StateOfMatch, float Warmup, float Match,
	float Cooldown, float StartingTime)
{
	WarmupTime = Warmup;
	MatchTime = Match;
	CooldownTime = Cooldown;
	LevelStartingTime = StartingTime;
	MatchState = StateOfMatch;
	OnMatchStateSet(MatchState);
	
	if (PlayerHUD && MatchState == MatchState::WaitingToStart)
	{
		PlayerHUD->AddMatchWaitingOverlay();
	}
}

////////////////////////////////////////////////////////////////////////////////////////
/// Start Sync Server Time
///

float ATW_PlayerController::GetServerTime()
{
	if (HasAuthority())
	{
		return GetWorld()->GetTimeSeconds();
	}
	
	return GetWorld()->GetTimeSeconds() + ClientServerDelta;
}

void ATW_PlayerController::ReceivedPlayer()
{
	Super::ReceivedPlayer();

	if (IsLocalController())
	{
		ServerRequestServerTime(GetWorld()->GetTimeSeconds());
	}
}

void ATW_PlayerController::CheckTimeSync(float DeltaTime)
{
	TimeSyncRunningTime += DeltaTime;
	if (IsLocalController() && TimeSyncRunningTime > TimeSyncFrequency)
	{
		ServerRequestServerTime(GetWorld()->GetTimeSeconds());
		TimeSyncRunningTime = 0.f;
	}
}

void ATW_PlayerController::ServerRequestServerTime_Implementation(float TimeOfClientRequest)
{
	const float ServerTimeOfReceipt = GetWorld()->GetTimeSeconds();
	
	ClientReportServerTime(TimeOfClientRequest, ServerTimeOfReceipt);
}

void ATW_PlayerController::ClientReportServerTime_Implementation(float TimeOfClientRequest,
                                                                 float TimeServerReceivedClientRequest)
{
	const float RoundTripTime = GetWorld()->GetTimeSeconds() - TimeOfClientRequest;
	const float CurrentServerTime = TimeServerReceivedClientRequest + (0.5f * RoundTripTime);
	ClientServerDelta = CurrentServerTime - GetWorld()->GetTimeSeconds();
}

///
/// End Section Sync Server Time
////////////////////////////////////////////////////////////////////////////////////////
