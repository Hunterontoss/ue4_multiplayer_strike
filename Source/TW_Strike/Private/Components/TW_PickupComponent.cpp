#include "Components/TW_PickupComponent.h"
#include "Components/SphereComponent.h"
#include "Components/WidgetComponent.h"
#include "Players/TW_BaseCharacter.h"
#include "Weapon/TW_BaseWeapon.h"

DEFINE_LOG_CATEGORY_STATIC(LogPickupComponent, All, All);

UTW_PickupComponent::UTW_PickupComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	
	const AActor* L_Owner = GetOwner();
	if (L_Owner)
	{
		AreaSphere = CreateDefaultSubobject<USphereComponent>(TEXT("PickupSphereComp"));
		AreaSphere->SetupAttachment(L_Owner->GetRootComponent());
		AreaSphere->SetCollisionResponseToChannels(ECollisionResponse::ECR_Ignore);
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		AreaSphere->SetSphereRadius(92.f);

		if (L_Owner->HasAuthority())
		{
			AreaSphere->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
			AreaSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
			AreaSphere->OnComponentBeginOverlap.AddDynamic(this, &UTW_PickupComponent::OnSphereOverlap);
			AreaSphere->OnComponentEndOverlap.AddDynamic(this, &UTW_PickupComponent::OnSphereEndOverlap);
		}
	}
	else
	{
		UE_LOG(LogPickupComponent, Warning, TEXT("Owner Not Found!!!"))
	}
}

void UTW_PickupComponent::BeginPlay()
{
	Super::BeginPlay();

	const ITW_InteractInterface* InteractInterface = Cast<ITW_InteractInterface>(GetOwner());
	checkf(InteractInterface, TEXT("Create Function For IT_WInteractInterface"))
	
}

void UTW_PickupComponent::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ATW_BaseCharacter* Player = Cast<ATW_BaseCharacter>(OtherActor);
	if (!Player) return;

	ATW_BaseWeapon* Weapon = GetOwner<ATW_BaseWeapon>();
	if (Weapon)
	{
		Player->SetOverlappingWeapon(Weapon);
	}
	
	ITW_InteractInterface* InteractInterface = Cast<ITW_InteractInterface>(GetOwner());
	if (InteractInterface && Player->IsLocallyControlled())
	{
		InteractInterface->SetPickupWidgetVisibility_Implementation(true);	
	}	
}

void UTW_PickupComponent::OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ATW_BaseCharacter* Player = Cast<ATW_BaseCharacter>(OtherActor);
	if (!Player) return;

	Player->SetOverlappingWeapon(nullptr);
	
	ITW_InteractInterface* InteractInterface = Cast<ITW_InteractInterface>(GetOwner());
	if (InteractInterface && Player->IsLocallyControlled())
	{
		InteractInterface->SetPickupWidgetVisibility_Implementation(false);	
	}
}
