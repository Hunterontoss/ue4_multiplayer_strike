#include "Components/TW_ZoomCameraComponent.h"

#include "Camera/CameraComponent.h"
#include "Players/TW_BaseCharacter.h"

UTW_ZoomCameraComponent::UTW_ZoomCameraComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	
}

void UTW_ZoomCameraComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner())
	{
		P_OwnerCharacter = GetOwner<ATW_BaseCharacter>();	
	}

	check(P_OwnerCharacter)

	if (P_OwnerCharacter->FollowCamera)
	{
		DefaultFOV = P_OwnerCharacter->FollowCamera->FieldOfView;
		CurrentFOV = DefaultFOV;
	}
}

void UTW_ZoomCameraComponent::InterpFOV(float DeltaTime, const float FOVZoom, const float InterpSpeedZoom)
{
	if (P_OwnerCharacter->IsAiming())
	{
		CurrentFOV = FMath::FInterpTo(CurrentFOV, FOVZoom, DeltaTime, InterpSpeedZoom);
	}
	else
	{
		CurrentFOV = FMath::FInterpTo(CurrentFOV, DefaultFOV, DeltaTime, ZoomInterpSpeed);
	}

	if (P_OwnerCharacter && P_OwnerCharacter->FollowCamera)
	{
		P_OwnerCharacter->FollowCamera->SetFieldOfView(CurrentFOV);
	}
}
