#include "Components/TW_AimOffsetCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Players/TW_BaseCharacter.h"
#include "Weapon/Components/TW_CombatComponent.h"

DEFINE_LOG_CATEGORY_STATIC(Log_AimOffset, All, All);

UTW_AimOffsetCharacter::UTW_AimOffsetCharacter()
{
	PrimaryComponentTick.bCanEverTick = false;

	TurningInPlace = ETurningInPlace::ETIP_NotTurning;
}
void UTW_AimOffsetCharacter::BeginPlay()
{
	Super::BeginPlay();

}

void UTW_AimOffsetCharacter::AimOffset(float DeltaTime)
{
	if (Character->CombatComponent && Character->CombatComponent->GetEquippedWeapon() == nullptr) return;
	
	const float Speed = CalculateSpeed();
	const bool bIsInAir = Character->GetCharacterMovement()->IsFalling();

	if (Speed == 0.f && !bIsInAir) // standing still, not jumping
	{
		bRotateRootBone = true;
		const FRotator CurrentAimRotation = FRotator(0.f, Character->GetBaseAimRotation().Yaw, 0.f);
		const FRotator DeltaAimRotation = UKismetMathLibrary::NormalizedDeltaRotator(CurrentAimRotation, StartingAimRotation);
		AO_Yaw = DeltaAimRotation.Yaw;

		if (TurningInPlace == ETurningInPlace::ETIP_NotTurning)
		{
			InterpAO_Yaw = AO_Yaw;
		}
		Character->bUseControllerRotationYaw = true;
		
		UpdateTurnInPlace(DeltaTime);
	}

	if (Speed > 0.f || bIsInAir) // running, or jumping
	{
		bRotateRootBone = false;
		StartingAimRotation = FRotator(0.f, Character->GetBaseAimRotation().Yaw, 0.f);
		AO_Yaw = 0.f;
		Character->bUseControllerRotationYaw = true;
		TurningInPlace = ETurningInPlace::ETIP_NotTurning;
	}
	
	CalculateAO_Pitch();
}

void UTW_AimOffsetCharacter::CalculateAO_Pitch()
{
	// Pitch   (Compress Rotation to 5 bytes on Character Movement )
	// CharacterMovementComponent -> CompressAxisToShort  [0, 360)
	AO_Pitch = Character->GetBaseAimRotation().Pitch;

	if (AO_Pitch > 90.f && !Character->IsLocallyControlled())
	{
		// map pitch from [270, 360) to [-90, 0)
		const FVector2D InRange = FVector2D(270.f, 360.f);
		const FVector2D OutRange = FVector2D(-90.f, 0.f);

		AO_Pitch = FMath::GetMappedRangeValueClamped(InRange, OutRange, AO_Pitch);
	}
}

void UTW_AimOffsetCharacter::UpdateTurnInPlace(float DeltaTime)
{
	if (AO_Yaw > 90.f)
	{
		TurningInPlace = ETurningInPlace::ETIP_Right;
	}
	else if (AO_Yaw < -90.f)
	{
		TurningInPlace = ETurningInPlace::ETIP_Left;
	}
	
	if (TurningInPlace != ETurningInPlace::ETIP_NotTurning)
	{
		InterpAO_Yaw = FMath::FInterpTo(InterpAO_Yaw, 0.f, DeltaTime, 4.f);

		AO_Yaw = InterpAO_Yaw;

		if (FMath::Abs(AO_Yaw) < 15.f)
		{
			TurningInPlace = ETurningInPlace::ETIP_NotTurning;
			StartingAimRotation = FRotator(0.f, Character->GetBaseAimRotation().Yaw, 0.f);
		}
	}	
}


void UTW_AimOffsetCharacter::SimProxiesTurn()
{
	if (!Character->CombatComponent || !Character->CombatComponent->GetEquippedWeapon()) return;
	
	bRotateRootBone = false;
	const float Speed = CalculateSpeed();
	
	if (Speed > 0.f)
	{
		TurningInPlace = ETurningInPlace::ETIP_NotTurning;
		return;
	}
	
	ProxyRotationLastFrame = ProxyRotation;
	ProxyRotation = Character->GetActorRotation();
	ProxyYaw = UKismetMathLibrary::NormalizedDeltaRotator(ProxyRotation, ProxyRotationLastFrame).Yaw;
	
	if (FMath::Abs(ProxyYaw) > TurnThreshold)
	{
		if (ProxyYaw > TurnThreshold)
		{
			TurningInPlace = ETurningInPlace::ETIP_Right;
		}
		else if (ProxyYaw < -TurnThreshold)
		{
			TurningInPlace = ETurningInPlace::ETIP_Left;
		}
		else
		{
			TurningInPlace = ETurningInPlace::ETIP_NotTurning;
		}
		return;
	}
	TurningInPlace = ETurningInPlace::ETIP_NotTurning;
}

float UTW_AimOffsetCharacter::CalculateSpeed()
{
	if (!Character)
	{
		UE_LOG(Log_AimOffset, Warning, TEXT("Charactert IsNull"))
	}
	
	FVector Velocity = Character->GetVelocity();
	Velocity.Z = 0.f;
	return Velocity.Size();
}

