#include "Components/TW_TraceComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Players/TW_BaseCharacter.h"
#include "Weapon/Components/TW_CombatComponent.h"
#include "Weapon/TW_BaseWeapon.h"

UTW_TraceComponent::UTW_TraceComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}


void UTW_TraceComponent::BeginPlay()
{
	Super::BeginPlay();	
}

void UTW_TraceComponent::TraceUnderCrosshairs(FHitResult& TraceHitResult)
{
	FVector CrosshairWorldPosition;
	FVector CrosshairWorldDirection;
	
	const bool bScreenToWorld = GetScreenToWorld(CrosshairWorldPosition, CrosshairWorldDirection);

	if (bScreenToWorld)
	{
		FVector Start = CrosshairWorldPosition;

		if (GetOwner())
		{
			const float DistanceToOwner = (GetOwner()->GetActorLocation() - Start).Size();
			Start += CrosshairWorldDirection * (DistanceToOwner + 1.f);
			DrawDebugSphere(GetWorld(), Start, 16.f, 12, FColor::Red, false);
		}

		const FVector End = Start + CrosshairWorldDirection * TW_TRACE_LENGTH;

		const FCollisionQueryParams L_CollisionParams = MakeQueryParams();

		GetWorld()->LineTraceSingleByChannel(TraceHitResult, Start, End, ECollisionChannel::ECC_Visibility, L_CollisionParams);
		if (!TraceHitResult.bBlockingHit)
		{
			TraceHitResult.ImpactPoint = End;
		}
	}

	
}

bool UTW_TraceComponent::GetScreenToWorld(FVector& CrosshairWorldPosition, FVector& CrosshairWorldDirection)
{
	FVector2D ViewportSize;
	if (GEngine && GEngine->GameViewport)
	{
		GEngine->GameViewport->GetViewportSize(ViewportSize);
	}

	const FVector2D CrosshairLocation(ViewportSize.X / 2.f, ViewportSize.Y / 2.f);
	
	const bool bScreenToWorld = UGameplayStatics::DeprojectScreenToWorld(
		UGameplayStatics::GetPlayerController(this, 0),
		CrosshairLocation,
		CrosshairWorldPosition,
		CrosshairWorldDirection
	);

	return bScreenToWorld;
}

FCollisionQueryParams UTW_TraceComponent::MakeQueryParams()
{
	FCollisionQueryParams L_CollisionParams;
	
	L_CollisionParams.AddIgnoredActor(GetOwner());

	auto* Character = GetOwner<ATW_BaseCharacter>();
	if (Character && Character->CombatComponent && Character->CombatComponent->GetEquippedWeapon())
	{
		L_CollisionParams.AddIgnoredActor(Character->CombatComponent->GetEquippedWeapon());		
	}

	return L_CollisionParams;
}

