#include "Components/TW_HealthComponent.h"
#include "GameModes/TW_StrikeGameMode.h"
#include "Net/UnrealNetwork.h"
#include "PlayerController/TW_PlayerController.h"
#include "Players/TW_BaseCharacter.h"

UTW_HealthComponent::UTW_HealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UTW_HealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTW_HealthComponent, Health);
}

void UTW_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	AActor* L_ComponentOwner = GetOwner();
	
	if (L_ComponentOwner && L_ComponentOwner->HasAuthority())
	{
		L_ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &UTW_HealthComponent::ReceiveDamage);
	}

}

void UTW_HealthComponent::ReceiveDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatorController, AActor* DamageCauser)
{
	Health = FMath::Clamp(Health - Damage, 0.f, MaxHealth);

	auto* L_Character = GetOwner<ATW_BaseCharacter>();
	if (L_Character)
	{
		L_Character->UpdateHUDHealth(Health, MaxHealth);
		L_Character->PlayHitReactMontage();
	}

	if (Health == 0.f && L_Character && L_Character->Controller)
	{
		auto* GameMode = GetWorld()->GetAuthGameMode<ATW_StrikeGameMode>();
		auto* PlayerController = Cast<ATW_PlayerController>(L_Character->Controller);
		if (GameMode)
		{
			auto* AttackerController = Cast<ATW_PlayerController>(InstigatorController);
			GameMode->PlayerEliminated(L_Character, PlayerController, AttackerController);
		}
	}
}

void UTW_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UTW_HealthComponent::OnRep_Health()
{
	auto* L_Character = GetOwner<ATW_BaseCharacter>();
	if (L_Character)
	{
		L_Character->UpdateHUDHealth(Health, MaxHealth);
		L_Character->PlayHitReactMontage();
	}
}
