// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerState/TW_PlayerState.h"

#include "Net/UnrealNetwork.h"
#include "PlayerController/TW_PlayerController.h"
#include "Players/TW_BaseCharacter.h"

void ATW_PlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(ATW_PlayerState, Defeats);
}

void ATW_PlayerState::AddToScore(float ScoreAmount)
{
	SetScore(GetScore() + ScoreAmount);
}

void ATW_PlayerState::AddToDefeats(int32 DefeatsAmount)
{
	Defeats += DefeatsAmount;
}

void ATW_PlayerState::OnRep_Score()
{
	Super::OnRep_Score();

	// TODO: MAKE observer
	
	if (GetPlayerControllerIsValid())
	{
		GetPlayerControllerIsValid()->SetHUDScore(GetScore());
	}
}

void ATW_PlayerState::OnRep_Defeats()
{
	if (GetPlayerControllerIsValid())
	{
		GetPlayerControllerIsValid()->SetHUDDefeats(GetDefeats());
	}
}

ATW_PlayerController* ATW_PlayerState::GetPlayerControllerIsValid() const
{
	const auto* L_Character = Cast<ATW_BaseCharacter>(GetPawn());

	if (L_Character && L_Character->Controller)
	{
		auto* L_Controller = Cast<ATW_PlayerController>(L_Character->Controller);

		if (L_Controller)
		{
			return L_Controller;
		}
	}
	return nullptr;
}

